# Goals

Sorted after priority

- [x] Provide video-metadata from Invidious (Model)
- [x] Playback from Youtube (View)
- [ ] Make playback always work
- [ ] Showing duration in gstreamer model
- [ ] Seek in video
- [ ] Video Quality options (for audio and video)
- [ ] Expose multiple video sources (DASH, 720p and 360p for Invidious)
- [ ] Recommendations
- [ ] Search
- [ ] Disable unused views and tag first version (0.alpha)
- [ ] Subscriptions
