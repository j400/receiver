# (Mostly Coding Style) Guidelines

The coding style currently differs a little bit in `src/logic` and `src`. I don't see a reason to make them more consistent, as I think of them as different components. If you want to change that or taking issue with any other aspect of the coding guidelines, open an issue and voice your thoughts, as its not clearly defined yet.

Some files may have false coding styles or wrong prefixes. If you're touching that file, could you please fix that? Thanks! 🧖️

## Common to all files

Adhere to the copyright statement style of the type of file you're editing
and add yourself to the names if you think you made a significant contribution.
Please try to copy it!
If you're working on an file and see something using an outdated style or could you please update it. Thanks! :).

### VIM modeline

If you're a vim user, then please add the modeline after the copyright header inside a seperate comment if not seperatly specified.

### Accessibility
Refer to the [GNOME Developer Documentation](https://developer.gnome.org/documentation/guidelines/accessibility.html). This should be high priority as soon we have a non-alpha release.
<!-- TODO: On Release: be accessible-->

## .c and .h
In general refer to the [GNOME C coding guidelines and look at the Linux kernel style](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html) if not specified clearly.

Especially the most important thing:
> check the surrounding code and try to imitate it

Things like tabs or spaces are probably different from these guidelines.

TODO: Verify that and define tab/spaces length for C files

### GObject classes
[GNOME C coding guidelines#GObject classes](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#gobject-classes)

### Namespacing
Generally refer to the [GNOME Coding Guidelines](https://developer.gnome.org/documentation/guidelines/programming/namespacing.html).

Specific to Receiver:

Prefix all (at least all GObject related files), if there isn't a good reason against it.
The file-prefix for Receiver is `rv-`.
The GObject prefix should be `Rv` and `RV_TYPE_`.
Exception: If it clears things up like the implementation of `RV_ITEM` (-> `rv-item-`, `RvItem` and `RV_ITEM_TYPE_`).


### Make clear if something is an View of an Model
Use the suffix `View` for an View of an Model.

Example:
- `RvVideoView` is the GUI component
- `RvVideo` is the model the service provides

### Add a Copyright header
I use GNOME Builder auto-completions which bowls down to just typing `agpl` and pressing ENTER while creating new files.
Refer to the same type of files in the same directory if you are in need of copying it.

- The first line should always be the document-name with its prefix and with a dot its file-type suffix.
- The next lines following should list all the persons with significant contributions.
- The license preamble can be ommited, just do the thing you prefer.
- The last line of the copyright header should always be the SPDX-License-Identifier.

Here an example:
```c
/* rv-random-gobject.h
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
```


### Header files
Prefer `#pragma once` otherwise refer to the [GNOME C Coding Guidelines about Header files](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#header-files)

### Ordering #include

Mostly ignored now.

This topic is a little bit more complicated but allows reading inclusions extremely easily.

Inclusion can have 6 different sections. Sort each alphabetically.

1. If you implement an interface and it's not already declared in the header file declaring the object, include them before.
2. Include the header file with the same name as the c file. (Most times the definition for your new GObject)
3. If you're including private header-files specific to this file, then have them next.

Blank line

4. If you're dealing with objects in header-files from `src/logic` (most times models) from the GUI, then have them next.

Blank line

5. Now start including the needed modules from different libraries like `GTK`, `Adwaita`, `Json-GLib`, `GLib`, `GIO`, `Soup`...

Blank line

6. At the bottom the different module from the own project.


#### Example for interfaces:
```c
/* ...
 * Copyright header end
 */

#include "rv-item.h"
#include "rv-item-video.h"

// C code begins
```
#### Example for a private thing, external and internal modules:
```c
/* ...
 * Copyright header end
 */

#include "rv-invidious.h"
#include "rv-invidious-private.h"

#include <gio/gio.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include "rv-video.h"

// C code begins
```

### Conditions
Basically from [GNOME C Coding Guidelines#Conditions](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#conditions).

Old code doesn't implement this yet, it boils down to not check for boolean values for equality and being verbose in checking for NULLs, 0, ...

### Functions
[GNOME C Coding Guidelines#Functions](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#functions)

### Struct Member Aligment
Old code aligns members of a struct like function arguments.
Example:
```c
struct _RvServiceInvidious
{
  GObject  parent_instance;

  /* invalid */
  gchar       *url;
  gboolean     ready;
  SoupSession *session;

  GHashTable *map_video;
};
```

New code shouldn't follow this anymore and should look like this:

```c
struct _RvServiceInvidious
{
  GObject  parent_instance;

  gchar *url;
  gboolean ready;
  SoupSession *session;

  GHashTable *map_video;
};
```

### Whitespace
[GNOME C Coding Guidelines for Whitespaces](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#whitespace)

### The `switch` statement
Probably the only case where we want to follow the GNU Style in the [GNOME C Coding Guidelines](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#the-switch-statement). All other thing from GNOME C Coding Guidelines about the `switch` statement apply as usual.

Example:
```c
switch (property_id)
  {
  case PROP_VIDEO:
    g_value_set_object (value, self->video);
    break;

  case PROP_STACK:
    g_value_set_object (value, self->content);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
```

### Memory allocation, Macros and Public API
- [GNOME C Coding Guidelines for Memory Allocation](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#memory-allocation)
- [GNOME C Coding Guidelines for Macros](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#macros)
- [GNOME C Coding Guidelines for Public API](https://developer.gnome.org/documentation/guidelines/programming/coding-style.html#public-api)

### FIXME: Documentation (not enforced yet)
Documentation comments in (transfer) annotation (as outlined in the [GNOME Coding Guidelines](https://developer.gnome.org/documentation/guidelines/programming/memory-management.html#documentation)

If you touch a function, then please add it in another commit.

TODO: We should consider documenting ownership aswell.

### FIXME: Memory managment (not enforced yet)
Looking at [GNOME Coding Guidelines` Memory Management](https://developer.gnome.org/documentation/guidelines/programming/memory-management.html) everything looks pretty sane.

TODO: Think more about memory management
<!--TODO: Read a few books from here https://developer.gnome.org/documentation/guidelines/programming/writing-good-code.html. And keep that here in mind while doing Recommendation Stuff: https://developer.gnome.org/documentation/guidelines/programming/optimizing.html -->

### Introspection
Not really relevant to Receiver (yet).
Here are the [GNOME Coding Guidelines about introspection](https://developer.gnome.org/documentation/guidelines/programming/introspection.html).

## TODO: Blueprints (not written yet)
A vim modeline already exists in some file 🧛️.
