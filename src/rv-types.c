/* rv-types.c
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <glib-2.0/glib.h>

#include "rv-application.h"
#include "rv-feeds.h"
#include "rv-item-view.h"
#include "rv-picture.h"
#include "rv-player.h"
#include "rv-preferences.h"
#include "rv-recommended.h"
#include "rv-tab.h"
#include "rv-tab-content.h"
#include "rv-time.h"
#include "rv-videoframe.h"
#include "rv-window.h"
#include "rv-videoview.h"

#include "logic/rv-feed.h"
#include "logic/rv-item.h"
#include "logic/rv-service.h"
#include "logic/rv-thumbnail.h"
#include "logic/rv-thumbnail-list.h"
#include "logic/rv-video.h"

#include "logic/invidious/rv-feed-invidious.h"
#include "logic/invidious/rv-invidious.h"
#include "logic/invidious/rv-item-invidious.h"
#include "logic/invidious/rv-video-invidious.h"

void
rv_init ()
{
  g_type_ensure (RV_TYPE_APPLICATION);
  g_type_ensure (RV_TYPE_FEEDS);
  g_type_ensure (RV_TYPE_ITEM_VIEW);
  g_type_ensure (RV_TYPE_PICTURE);
  g_type_ensure (RV_TYPE_PLAYER);
  g_type_ensure (RV_TYPE_PREFERENCES);
  g_type_ensure (RV_TYPE_RECOMMENDED);
  g_type_ensure (RV_TYPE_TAB);
  g_type_ensure (RV_TYPE_TAB_CONTENT);
  g_type_ensure (RV_TYPE_TIME);
  g_type_ensure (RV_TYPE_VIDEOFRAME);
  g_type_ensure (RV_TYPE_WINDOW);
  g_type_ensure (RV_TYPE_VIDEO_VIEW);

  g_type_ensure (RV_TYPE_FEED);
  g_type_ensure (RV_TYPE_ITEM);
  g_type_ensure (RV_TYPE_SERVICE);
  g_type_ensure (RV_TYPE_THUMBNAIL);
  g_type_ensure (RV_TYPE_THUMBNAIL_LIST);

  g_type_ensure (RV_TYPE_INVIDIOUS);
  g_type_ensure (RV_TYPE_FEED_INVIDIOUS);
  g_type_ensure (RV_TYPE_ITEM_INVIDIOUS);
  g_type_ensure (RV_TYPE_VIDEO_INVIDIOUS);
}
