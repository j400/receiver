/* rv-application.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Receiver.Application"

#include "rv-application.h"

#include <gst/gst.h>

#include "rv-window.h"

struct _RvApplication
{
  GtkApplication parent_instance;

  GSettings          *settings;

  guint active_app_window_id;
};

G_DEFINE_TYPE (RvApplication, rv_application, ADW_TYPE_APPLICATION)

void on_open_with_uri (gpointer data, gpointer user_data);

static void
rv_application_dispose (GObject *gobject)
{
  RvApplication *self = RV_APPLICATION (gobject);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (rv_application_parent_class)->dispose (gobject);
}

RvApplication *
rv_application_new (gchar *application_id,
                    GApplicationFlags  flags)
{
  return g_object_new (RV_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
rv_application_finalize (GObject *object)
{
  RvApplication *self = RV_APPLICATION (object);

  G_OBJECT_CLASS (rv_application_parent_class)->finalize (object);
}

static void
rv_application_startup (GApplication *app){
  GstElementFactory *factory = NULL;
  gboolean missing_plugins = FALSE;

  G_APPLICATION_CLASS (rv_application_parent_class)->startup(app);

  {
    factory = gst_element_factory_find ("gtk4paintablesink");
    if (!factory) {
      g_critical ("gstgtk4/gst-plugin-gtk4 from gst-plugins-rust is missing.");
      missing_plugins = TRUE;
    } else {
      g_debug ("gtk4paintablesink is here!");
    }
    g_object_unref (factory);
    factory = NULL;
  }

  {
    factory = gst_element_factory_find ("dashdemux2");
    if (!factory) {
      g_critical ("adaptivedemuxers2/dashdemux2 from gst-plugins-good is missing.");
      missing_plugins = TRUE;
    } else {
      g_debug ("dashdemux2 is here!");
    }
    g_object_unref (factory);
    factory = NULL;
  }

  if (missing_plugins) {
    g_error ("Missing gstreamer plugins, please install them. This app won't work without them.'");
  }
}

RvWindow*
rv_application_activate_normal (RvApplication* app)
{
  RvWindow *window = rv_window_new (app);

  g_settings_bind (app->settings, "window-width",
                   G_OBJECT (window), "default-width",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (app->settings, "window-height",
                   G_OBJECT (window), "default-height",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (app->settings, "window-maximized",
                   G_OBJECT (window), "maximized",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_set_boolean (app->settings, "first-run", false);

  return window;
}

RvWindow *
rv_application_activate (GApplication *app)
{
  RvWindow *window = NULL;

  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));

  /* Get the last app window or create one if necessary. */
  GList *list = gtk_application_get_windows (GTK_APPLICATION (app));
  for (; list; list = list->next) {
    if (RV_IS_WINDOW (list->data)) {
      window = RV_WINDOW (list->data);
    }
  }
  if (window == NULL) {
      window = rv_application_activate_normal (RV_APPLICATION (app));
  }

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (GTK_WINDOW (window));
  return window;
}

void
rv_application_open (GApplication  *application,
                     GFile        **files,
                     gint           n_files,
                     gchar         *hint)
{
  RvApplication *app = RV_APPLICATION (application);
  GError *error = NULL;

  RvWindow *window = rv_application_activate (app);

  GFile *file;
  for (int i = 0; i < n_files; i++) {
    file = files[i];
    if (file == NULL || !G_IS_FILE (file)) {
      continue;
    }

    gchar *scheme = g_file_get_uri_scheme (file);
    g_info ("rv_application_open: parsing URI_scheme: %s", scheme);
    if (!g_str_equal ("receiver", scheme)) {
      g_warning ("rv_application_open: Not a URI with the 'receiver' scheme");
      g_free (scheme);
      continue;
    }
    g_free (scheme);

    gchar *uri_raw = g_file_get_uri (file);
    g_info ("rv_application_open: parsing URI_Raw: %s", uri_raw);
    GUri *uri = g_uri_parse (uri_raw, G_URI_FLAGS_NONE, &error);
    if (error) {
      continue;
    }
    g_free (uri_raw);

    const gchar *path;
    path = g_uri_get_path (uri);
    g_info ("URI: parsing URI_path: %s", path);
    if (path == NULL || strlen(path) == 0){
      g_warning ("receiver: path invalid");
      g_uri_unref (uri);
      continue;
    }
    rv_window_new_tab_with_uri (window, uri);
  }
};

static void
rv_application_class_init (RvApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = rv_application_finalize;
  object_class->dispose = rv_application_dispose;

  app_class->activate = rv_application_activate;
  app_class->startup = rv_application_startup;
}

static void
rv_application_init (RvApplication *self)
{
  g_signal_connect (self, "open", G_CALLBACK (rv_application_open), NULL);

  self->settings = g_settings_new ("app.drey.Receiver");
}
