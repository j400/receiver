/* rv-tab.c
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-tab.h"

#include "logic/rv-errors.h"
#include "logic/rv-service.h"
#include "logic/rv-video.h"
#include "logic/invidious/rv-invidious.h"

#include "rv-videoview.h"
#include "rv-tab-content.h"
#include "rv-window.h"
#include "rv-application.h"

struct _RvTab
{
  AdwBin parent_instance;
  GtkStack *stack;

  AdwTabPage *tab_page;
  GtkExpressionWatch *tab_page_title_watch;

  GCancellable *open_action_cancellable;

  GSettings *settings;
  gchar *title;
  GList *history;
};

G_DEFINE_FINAL_TYPE (RvTab, rv_tab, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_TAB_PAGE,
  PROP_TITLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

RvTab *
rv_tab_new ()
{
  return g_object_new (RV_TYPE_TAB,
                       NULL);
}

static void
rv_tab_finalize (GObject *object)
{
  RvTab *self = (RvTab *)object;

  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (rv_tab_parent_class)->finalize (object);
}

static void
rv_tab_dispose (GObject *object)
{
  RvTab *self = (RvTab *)object;

  g_clear_object (&self->tab_page);
  g_clear_object (&self->open_action_cancellable);

  G_OBJECT_CLASS (rv_tab_parent_class)->dispose (object);
}

static void
rv_tab_get_property (GObject    *object,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
  RvTab *self = RV_TAB (object);

  switch (prop_id)
    {
    case PROP_TAB_PAGE:
      g_value_set_object (value, self->tab_page);
      break;
    case PROP_TITLE:
      g_value_set_string (value, self->title);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_tab_set_property (GObject      *object,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  RvTab *self = RV_TAB (object);

  switch (prop_id)
    {
    case PROP_TAB_PAGE:
      if (self->tab_page_title_watch) {
          gtk_expression_watch_unwatch (self->tab_page_title_watch);
          return;
      }
      self->tab_page = g_value_dup_object (value);
      GtkExpression *title_expre = gtk_property_expression_new (RV_TYPE_TAB, NULL, "title");
      self->tab_page_title_watch = gtk_expression_bind (title_expre, self->tab_page, "title", self);
      break;
    case PROP_TITLE:
      if (self->title != NULL) {
        g_clear_pointer (&self->title, g_free);
      }
      self->title = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_tab_action_open_uri (GtkWidget  *widget,
                        const char *action_name,
                        GVariant   *parameter)
{
  g_assert (RV_IS_TAB (widget));
  RvTab *self = RV_TAB (widget);
  GError *error = NULL;
  GUri *uri;
  gsize length;

  const gchar *uri_raw = g_variant_get_string (parameter, &length);
  gchar *dest = g_strdup (uri_raw);
  g_info ("tab.open: %s", dest);
  uri = g_uri_parse (dest, G_URI_FLAGS_NONE, &error);

  rv_tab_open_uri (self, uri);
  g_free (dest);
}

void
rv_tab_action_history_next (RvTab      *self,
                            const char *action_name,
                            GVariant   *parameter);
void
rv_tab_action_history_previous (RvTab      *self,
                                const char *action_name,
                                GVariant   *parameter);
static void
rv_tab_class_init (RvTabClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = rv_tab_dispose;
  object_class->finalize = rv_tab_finalize;
  object_class->get_property = rv_tab_get_property;
  object_class->set_property = rv_tab_set_property;

  properties [PROP_TAB_PAGE] =
    g_param_spec_object ("tab-page", NULL, NULL,
                         G_TYPE_OBJECT,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_TITLE] =
    g_param_spec_string ("title", NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/app/drey/Receiver/ui/rv-tab.ui");
  gtk_widget_class_bind_template_child (widget_class, RvTab, stack);

  gtk_widget_class_install_action (widget_class, "tab.open", NULL, rv_tab_action_open_uri);
  gtk_widget_class_install_action (widget_class, "tab.history.previous", NULL, rv_tab_action_history_previous);
  gtk_widget_class_install_action (widget_class, "tab.history.next", NULL, rv_tab_action_history_next);
}

static void
rv_tab_init (RvTab *self)
{
  self->title = NULL;
  self->tab_page_title_watch = NULL;
  self->history = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("app.drey.Receiver");

  GtkExpression *visible_child_expre = gtk_property_expression_new (GTK_TYPE_STACK, NULL, "visible-child");
  GtkExpression *title_expre = gtk_property_expression_new (RV_TYPE_TAB_CONTENT, visible_child_expre, "title");
  gtk_expression_bind (title_expre, self, "title", self->stack);
}

static void
rv_tab_update_history_actions (RvTab *self)
{
  gtk_widget_action_set_enabled (GTK_WIDGET (self), "tab.history.previous", self->history && self->history->prev);
  gtk_widget_action_set_enabled (GTK_WIDGET (self), "tab.history.next", self->history && self->history->next);
}


void
rv_tab_open_video (RvTab *self, RvVideo *vid)
{
  RvVideoView *page_video = (RvVideoView*) gtk_stack_get_child_by_name (self->stack, "video");

  if (page_video == NULL) {
    page_video = rv_video_view_new (vid);
    gtk_stack_add_named (self->stack, page_video, "video");
  } else {
    g_assert (RV_IS_VIDEO_VIEW (page_video));
    rv_video_view_set_video (page_video, vid);
  }

  gtk_stack_set_visible_child (self->stack, GTK_WIDGET (page_video));
}

void
free_list (GList *list)
{
  while (list) {
    g_object_unref (list->data);
    list = list->next;
  }
}

void
rv_tab_open_new_vid (RvTab *self, RvVideo *vid)
{
  g_object_ref (vid);

  if (self->history) {
    GList *old_next = g_list_next (self->history);
    self->history->next = NULL;
    if (old_next) {
      old_next->prev = NULL;
      free_list (old_next);
      g_list_free (old_next);
    }
  }

  self->history = g_list_append (self->history, vid);
  if (self->history->next) {
    self->history = self->history->next;
  }

  rv_tab_update_history_actions (self);
  rv_tab_open_video (self, vid);
}

typedef struct {const gchar* key; void (*func)(RvTab*, GUri*);} HandlerMap;
typedef struct {const gchar* key; void (*func)(RvTab*, GUri*, const gchar*);} ServiceHandlerMap;

gchar*
cb_get_invidious_instance_uri (RvTab *self)
{
  g_assert (RV_IS_TAB (self));
  return g_settings_get_string (self->settings, "invidious-url");
}

struct _CBStructOnOpenServiceYoutubeVideo {
  RvTab *self;
  gchar *path;
};
typedef struct _CBStructOnOpenServiceYoutubeVideo CBStructOnOpenServiceYoutubeVideo;
static void
cb_on_open_service_youtube_video (GObject*, GAsyncResult*,
                                  CBStructOnOpenServiceYoutubeVideo*);

static void
invidious_instance_url_supplied (GObject *,
                                 CBStructOnOpenServiceYoutubeVideo *struc)
{
  rv_invidious_get_default_async (struc->self, TRUE, NULL,
                                  cb_get_invidious_instance_uri,
                                  cb_on_open_service_youtube_video,
                                  struc);
}

static void
invidious_instance_url_required (RvTab *self,
                                 CBStructOnOpenServiceYoutubeVideo *struc)
{
  // TODO: Show toast explaining the situation, mark EntryRow with an error style, navigate to the invidious page
  RvWindow *window = rv_window_get_by_widget (self);
  RvPreferences *pref = rv_window_get_preferences_window (self);
  gtk_window_present (GTK_WINDOW (pref));
  g_signal_connect (pref, "invidious-url-apply", invidious_instance_url_supplied, struc);
}

static void
cb_on_open_service_youtube_video_ready (RvServiceInvidious *service,
                                        GAsyncResult *result,
                                        RvTab *self)
{
  GError *error = NULL;
  RvServiceInterface *iface = RV_SERVICE_GET_IFACE (service);
  RvVideo *video;
  g_assert (RV_IS_TAB (self));

  video = iface->new_video_finish (service, result, &error);
  if (error){
    if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
      return;
    } else {
      // TODO: User friendly error toast
      g_error ("Received error after trying to open a video: %s", error->message);
      g_abort ();
    }
  }
  if (video == NULL) {
    g_error ("cb_on_open_service_youtube_video_ready: Task was invalid, WTF");
    g_abort ();
  }

  rv_tab_open_new_vid (self, video);
}

static void
cb_on_open_service_youtube_video (GObject *source_object,
                                  GAsyncResult *result,
                                  CBStructOnOpenServiceYoutubeVideo *user_data)
{
  GError *error = NULL;
  g_assert (RV_IS_INVIDIOUS (source_object));
  RvServiceInvidious *service = source_object;
  RvServiceInterface *iface = RV_SERVICE_GET_IFACE (service);
  gchar *path = user_data->path;
  RvTab *self = user_data->self;

  // only free the struc if we actually succeed
  rv_invidious_get_default_finish (source_object, result, &error);
  /* FIXME: for some reason we get   rv_application_new_tab_video (self, video);an empty error obj??!? */
  if (g_task_had_error (G_TASK (result))) {
    if (g_error_matches (error, RV_SERVICE_INIT_ERROR, RV_SERVICE_INIT_ERROR_INVALID_URI)) {
      g_object_unref (service);
      invidious_instance_url_required (self, user_data);
      return;
    }
    g_assert_not_reached ();
  }
  iface->new_video_async (service, path, 0, NULL, cb_on_open_service_youtube_video_ready, self);


  g_free (path);
  g_free (user_data);
}

static void
on_open_service_youtube_video (RvApplication *self, GUri *uri, const gchar *path)
{
  g_assert_nonnull (path);
  CBStructOnOpenServiceYoutubeVideo *struc =
    g_new (CBStructOnOpenServiceYoutubeVideo, 1);
  struc->self = self;
  struc->path = g_strdup (path);

  // TODO: add more callbacks to set preferences
  rv_invidious_get_default_async (self,
                                  FALSE,
                                  NULL,
                                  cb_get_invidious_instance_uri,
                                  cb_on_open_service_youtube_video,
                                  struc);
}

#define SERVICE_YOUTUBE_HANDLER_LENGTH 4
static ServiceHandlerMap service_youtube_handler[SERVICE_YOUTUBE_HANDLER_LENGTH] =
{
    {"video", on_open_service_youtube_video},
};
/*
 * :service//youtube/video/[youtube video id]
 * :service//youtube/playlist/[youtube playlist id]
 * :service//youtube/channel/[youtube channel id]
 * :service//youtube/feed/trending
 * :service//youtube/feed/music
 * :service//youtube/feed/moviesandshows # doesn't make any sense in this application, will probably be an easteregg
 * :service//youtube/feed/live
 * :service//youtube/feed/gaming
 * :service//youtube/feed/news
 * :service//youtube/feed/sports
 * :service//youtube/feed/learning
 * :service//youtube/search/general?q:query // FIXME: SearchAPI parameters like filters or length?
 * :service//youtube/search/channel?q:query
*/

static void
on_open_service_youtube (RvTab       *self,
                         GUri        *uri,
                         const gchar *path)
{
  if (path == NULL) return;

  gchar **splits = g_strsplit (path, "/", 2);
  if (splits == NULL) return;
  if (splits[0] == NULL) return;

  for (int try = 0; try < SERVICE_YOUTUBE_HANDLER_LENGTH; try++){
    if (g_str_equal (service_youtube_handler[try].key, splits[0])) {
      service_youtube_handler[try].func(self, uri, splits[1]);
      break;
    }
  }

  g_strfreev(splits);
}

#define SERVICE_HANDLER_LENGTH 2
static ServiceHandlerMap service_handler[SERVICE_HANDLER_LENGTH] =
{
    {"youtube", on_open_service_youtube},
};

static void
on_open_service (RvTab       *self,
                 GUri        *uri,
                 const gchar *path)
{
  gchar **splits = g_strsplit (path, "/", 2);

  g_info ("Using Service Handler");
  if (splits == NULL) return;
  if (splits[0] == NULL) return;

  for (int try = 0; try < SERVICE_HANDLER_LENGTH; try++){
    if (g_str_equal (service_handler[try].key, splits[0])) {
      service_handler[try].func(self, uri, splits[1]);
      break;
    }
  }

  g_strfreev(splits);
}

#define HOST_HANDLER_LENGTH 1
static ServiceHandlerMap host_handler[HOST_HANDLER_LENGTH] =
{
    {"service", on_open_service},
};

void
rv_tab_do_uri (RvTab *self, GUri *uri)
{
  const gchar *path = g_uri_get_path (uri);
  gchar **splits = g_strsplit (path, "//", 2);
  if (splits == NULL || splits[0] == NULL) {
    g_error ("URI: uri-splits no workey?");
    g_strfreev (splits);
    return;
  }
  for (int u = 0; u < HOST_HANDLER_LENGTH; u++) {
    if (g_str_equal (host_handler[u].key, splits[0])){
      host_handler[u].func(self, uri, splits[1]);
      break;
    }
  }
  g_strfreev (splits);
}

void
rv_tab_open_uri (RvTab *self, GUri *uri)
{
  g_assert (RV_IS_TAB (self));

  rv_tab_do_uri (self, uri);
}

void
rv_tab_action_history_common (RvTab *self,
                              GList *requested)
{
  /* Shouldn't have been called if no previous/next history exists */
  g_assert_nonnull (requested);
  g_assert_nonnull (requested->data);

  // TODO: Move self->history to HistoryObject, which tracks states of the views
  self->history = requested;
  rv_tab_update_history_actions (self);

  if (RV_IS_VIDEO (requested->data)) {
    rv_tab_open_video (self, requested->data);
  } else {
    g_assert_not_reached ();
  }
}


void
rv_tab_action_history_previous (RvTab      *self,
                                const char *action_name,
                                GVariant   *parameter)
{
  g_assert (RV_IS_TAB (self));
  g_assert_nonnull (self->history);

  rv_tab_action_history_common (self, self->history->prev);
}

void
rv_tab_action_history_next (RvTab      *self,
                            const char *action_name,
                            GVariant   *parameter)
{
  g_assert (RV_IS_TAB (self));
  g_assert_nonnull (self->history);

  rv_tab_action_history_common (self, self->history->next);
}

