/* rv-tab.h
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <adwaita.h>

#include <logic/rv-video.h>

G_BEGIN_DECLS

#define RV_TYPE_TAB (rv_tab_get_type())

G_DECLARE_FINAL_TYPE (RvTab, rv_tab, RV, TAB, AdwBin)

RvTab *rv_tab_new ();
void rv_tab_open_uri (RvTab *self, GUri *uri);

G_END_DECLS
