/* rv-player.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Receiver.Player"

#include "rv-player.h"
#include "rv-player-private.h"

#include "logic/rv-video.h"

#include <adwaita.h>
#include <gtk/gtk.h>
#include <gst/gst.h>

#include "rv-videoframe.h"

struct _RvPlayer
{
  AdwBin parent_instance;

  GtkStack *stack;
  GtkOverlay *overlay;
  RvVideoFrame *frame;

  GtkStack *centerstack;
  GtkSpinner *loadingspinner;
  GtkButton *startplaybutton;
  GtkBox   *centerbuttons_normal_box;
  GtkStack *playbackstack;
  GtkButton *playbutton;
  GtkButton *pausebutton;
  GtkBox   *centerbuttons_eos_box;

  GtkBox *title_pill;
  GtkLabel *title_pill_title;
  GtkLabel *title_pill_author;
  GtkButton *title_pill_description_button;

  GtkBox *description_pill;
  GtkLabel *description_pill_title;
  GtkLabel *description_pill_author;
  GtkLabel *description_pill_description_text;
  GtkButton *description_pill_close_btn;

  GtkBox *bottomcontrolsbox;
  AdwTimedAnimation *auto_hide;
  double controls_opacity;
  bool controls_enable;
  GSource *timeout_source_hide_animation_starter;

  UIState ui_state;
  PlayerState player_state;
  PlaybackState playback_state;
};

G_DEFINE_FINAL_TYPE (RvPlayer, rv_player, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_SRC,
  PROP_READY,
  PROP_CONTROLS_OPACITY,
  PROP_CONTROLS_ENABLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

RvPlayer *
rv_player_new (void)
{
  return g_object_new (RV_TYPE_PLAYER, NULL);
}

static void
rv_player_finalize (GObject *object)
{
  RvPlayer *self = (RvPlayer *)object;

  G_OBJECT_CLASS (rv_player_parent_class)->finalize (object);
}

static void
rv_player_dispose (GObject *object)
{
  RvPlayer *self = (RvPlayer *)object;

  g_clear_object (&self->auto_hide);

  G_OBJECT_CLASS (rv_player_parent_class)->dispose (object);
}

static void
rv_player_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  RvPlayer *self = RV_PLAYER (object);

  switch (prop_id)
    {
    case PROP_READY:
      g_object_get_property (G_OBJECT (self->frame), "ready", value);
      break;
    case PROP_SRC:
      g_object_get_property (G_OBJECT (self->frame), "src", value);
      break;
    case PROP_CONTROLS_ENABLE:
      g_value_set_boolean (value, self->controls_enable);
      break;
    case PROP_CONTROLS_OPACITY:
      g_value_set_double (value, self->controls_opacity);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_player_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  RvPlayer *self = RV_PLAYER (object);

  switch (prop_id)
    {
    case PROP_SRC:
      g_object_set_property (G_OBJECT (self->frame), "src", value);
      break;
    case PROP_CONTROLS_ENABLE:
      self->controls_enable = g_value_get_boolean (value);
      break;
    case PROP_CONTROLS_OPACITY:
      self->controls_opacity = g_value_get_double (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_player_class_init (RvPlayerClass *klass)
{
  GObjectClass *class_obj = G_OBJECT_CLASS (klass);

  class_obj->finalize = rv_player_finalize;
  class_obj->dispose = rv_player_dispose;
  class_obj->get_property = rv_player_get_property;
  class_obj->set_property = rv_player_set_property;

  properties[PROP_SRC] =
    g_param_spec_object ("src", NULL, NULL,
                         RV_TYPE_VIDEO,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));


  properties[PROP_READY] =
    g_param_spec_boolean ("ready", NULL, NULL,
                          FALSE,
                          (G_PARAM_READABLE |
                           G_PARAM_EXPLICIT_NOTIFY |
                           G_PARAM_STATIC_STRINGS));

  properties[PROP_CONTROLS_OPACITY] =
    g_param_spec_double ("controls-opacity", NULL, NULL,
                         0, 1, 1,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_CONTROLS_ENABLE] =
    g_param_spec_boolean ("controls-enable", NULL, NULL,
                          TRUE,
                          (G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT |
                           G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (class_obj, N_PROPS, properties);

  GtkWidgetClass *class_widget = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (class_widget, "/app/drey/Receiver/ui/rv-player.ui");

  gtk_widget_class_bind_template_child (class_widget, RvPlayer, overlay);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, frame);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, centerstack);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, loadingspinner);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, startplaybutton);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, centerbuttons_normal_box);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, playbackstack);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, playbutton);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, pausebutton);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, centerbuttons_eos_box);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, bottomcontrolsbox);

  gtk_widget_class_bind_template_child (class_widget, RvPlayer, title_pill);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, title_pill_description_button);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, title_pill_title);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, title_pill_author);

  gtk_widget_class_bind_template_child (class_widget, RvPlayer, description_pill);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, description_pill_close_btn);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, description_pill_title);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, description_pill_author);
  gtk_widget_class_bind_template_child (class_widget, RvPlayer, description_pill_description_text);

}

void
disable_ui_state_player (RvPlayer *self)
{

  gtk_widget_set_visible (self->bottomcontrolsbox, FALSE);
  gtk_widget_set_visible (self->centerstack, FALSE);
  gtk_widget_set_visible (self->title_pill, FALSE);
}

void
disable_ui_state_description (RvPlayer *self)
{
  gtk_widget_set_visible (self->description_pill, FALSE);
}

void
enable_ui_state_player (RvPlayer *self)
{
  gtk_widget_set_visible (self->centerstack, TRUE);
  gtk_widget_set_visible (self->title_pill, TRUE);
  gtk_widget_set_visible (self->bottomcontrolsbox, FALSE);

  switch (self->player_state) {
    case STATE_PLAYER_READY:
      gtk_stack_set_visible_child(self->centerstack, self->startplaybutton);
      break;

    case STATE_PLAYER_IN_PLAY:
      gtk_stack_set_visible_child(self->centerstack, self->centerbuttons_normal_box);
      gtk_widget_set_visible (self->bottomcontrolsbox, TRUE);
      break;

    case STATE_PLAYER_EOS:
      gtk_stack_set_visible_child(self->centerstack, self->centerbuttons_eos_box);
      break;
  }

}

void
enable_ui_state_description (RvPlayer *self)
{
  gtk_widget_set_visible (self->description_pill, TRUE);
}

void
switch_state_from_loading (RvPlayer *self, UIState new)
{}

void
switch_state_to_loading (RvPlayer *self)
{
  gtk_stack_set_visible_child(self->centerstack, GTK_WIDGET (self->loadingspinner));
}

void
_change_ui_state (RvPlayer *self, UIState old, UIState new)
{
  // disable old state
  switch (old) {
    case STATE_UI_LOADING:
      switch_state_from_loading (self, new);
      break;

    case STATE_UI_PLAYER:
      disable_ui_state_player (self);
      break;

    case STATE_UI_DESCRIPTION:
      disable_ui_state_description (self);
      break;

    case STATE_UI_NULL:
      break;

    default:
      g_critical ("Transition from unknown UI state");
      break;
  }

  // send warning for unexpected trans
  if (old == STATE_UI_LOADING &&
      (new == STATE_UI_DESCRIPTION)) {
    g_warning("change_ui_state: didn't expect transition from STATE_UI_LOADING to STATE_UI_DESCRIPTION");
  }

  // enable new state
  switch (new) {
    case STATE_UI_PLAYER:
      enable_ui_state_player (self);
      break;

    case STATE_UI_DESCRIPTION:
      enable_ui_state_description (self);
      break;

    case STATE_UI_LOADING:
      switch_state_to_loading (self);
      break;

    case STATE_UI_NULL:
      enable_ui_state_null (self);
      break;

    default:
      g_critical ("Transition to unknown UI state");
      break;
  }

  self->ui_state = new;
}

void
change_ui_state (RvPlayer *self, UIState new) {
  UIState old = self->ui_state;

  if (old == new) {
    return;
  }

  _change_ui_state (self, old, new);
}

void
change_playing_state (RvPlayer *self, PlaybackState new, gboolean sync_gstreamer)
{
  PlaybackState old = self->playback_state;
  GstElement *bin = GST_ELEMENT (rv_videoframe_get_pipeline (self->frame));

  if (old == new) {
    return;
  }

  switch (new) {
    case STATE_PLAYBACK_NULL:
      break;

    case STATE_PLAYBACK_PAUSED:
      if (sync_gstreamer) {
        gst_element_call_async (bin, gst_element_set_state, GST_STATE_PAUSED, NULL);
      }
      gtk_stack_set_visible_child (self->playbackstack, GTK_WIDGET (self->playbutton));
      break;

    case STATE_PLAYBACK_PLAYING:
      if (sync_gstreamer) {
        gst_element_call_async (bin, gst_element_set_state, GST_STATE_PLAYING, NULL);
      }
      gtk_stack_set_visible_child (self->playbackstack, GTK_WIDGET (self->pausebutton));
      break;

    default:
      g_critical ("Transition to unknown PlaybackState");
      break;
  }

  self->playback_state = new;
}

static void
on_ready_cb (RvVideoFrame* frame, gpointer, RvPlayer *self)
{
  g_debug ("Ready_cb rv-player called!");
  gboolean ready;
  g_object_get (frame,
                "ready", &ready,
                NULL);

  if(ready){
    change_ui_state (self, STATE_UI_PLAYER);
  } else {
    g_assert_not_reached ();
  }

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_READY]);
}

static void
on_playbutton_clicked (GtkButton *button,
                       gpointer   user_data)
{
  g_assert (RV_IS_PLAYER (user_data));
  RvPlayer *self = RV_PLAYER (user_data);

  g_info ("Trying to play a video");

  change_playing_state (self, STATE_PLAYBACK_PLAYING, TRUE);
}

static void
on_pausebutton_clicked (GtkButton *,
                        gpointer   user_data)
{
  g_assert (RV_IS_PLAYER (user_data));
  RvPlayer *self = RV_PLAYER (user_data);

  change_playing_state (self, STATE_PLAYBACK_PAUSED, TRUE);
  cancel_timeout (self);
}

void
on_animation_done (AdwAnimation *anim,
                RvPlayer     *self)
{
  g_object_set (self, "controls-enable", false, NULL);
}

void
reset_animation (RvPlayer *self) {
  adw_animation_reset (self->auto_hide);
  g_object_set (self, "controls-enable", TRUE, "controls-opacity", 1., NULL);
}

gboolean
timeout_reached (RvPlayer *self)
{
  adw_animation_play (self->auto_hide);

  // returing false, so this source gets removed
  self->timeout_source_hide_animation_starter = NULL;
  return FALSE;
}

// FIXME: error logging
void
start_timeout (RvPlayer *self)
{
  g_assert (self->timeout_source_hide_animation_starter == NULL);
  self->timeout_source_hide_animation_starter = g_timeout_source_new_seconds (5);
  g_source_set_callback (self->timeout_source_hide_animation_starter,
                         timeout_reached,
                         self,
                         NULL);

  g_source_attach (self->timeout_source_hide_animation_starter, g_main_context_default ());
}

void
cancel_timeout (RvPlayer *self)
{
  if (self->timeout_source_hide_animation_starter != NULL) {
    g_source_destroy (self->timeout_source_hide_animation_starter);
    self->timeout_source_hide_animation_starter = NULL;
  }
}

void
restart_timeout (RvPlayer *self)
{
  cancel_timeout (self);
  start_timeout (self);
}

static void
on_show_description_clicked (GtkButton *button,
                             gpointer   user_data)
{
  RvPlayer *self;

  g_assert (RV_IS_PLAYER (user_data));
  self = RV_PLAYER (user_data);

  if (self->ui_state == STATE_UI_PLAYER) {
    if (self->playback_state == STATE_PLAYBACK_PLAYING) {
      change_playing_state (self, STATE_PLAYBACK_PAUSED, TRUE);
    }
    change_ui_state (self, STATE_UI_DESCRIPTION);
  }
}

static void
on_state_play (gpointer, gpointer user_data)
{
  g_assert (RV_IS_PLAYER (user_data));
  RvPlayer *self = RV_PLAYER (user_data);

  self->player_state = STATE_PLAYER_IN_PLAY;

  change_playing_state (self, STATE_PLAYBACK_PLAYING, FALSE);
  // we're forcing reconfiguring by the state machine here
  _change_ui_state (self, STATE_UI_NULL, STATE_UI_PLAYER);

  restart_timeout (self);
}

static void
on_state_pause (gpointer, gpointer user_data)
{
  g_assert (RV_IS_PLAYER (user_data));
  RvPlayer *self = RV_PLAYER (user_data);

  PlayerState old = self->player_state;
  self->player_state = STATE_PLAYER_IN_PLAY;

  change_playing_state (self, STATE_PLAYBACK_PAUSED, FALSE);
  if (self->ui_state != STATE_UI_DESCRIPTION) {
    change_ui_state (self, STATE_UI_PLAYER);
  }
  if (old != self->player_state) {
    // we're forcing reconfiguring by the state machine here
    _change_ui_state (self, STATE_UI_NULL, STATE_UI_PLAYER);
  }

  cancel_timeout (self);
}

static void
on_state_eos (gpointer, gpointer user_data)
{
  g_assert (RV_IS_PLAYER (user_data));
  RvPlayer *self = RV_PLAYER (user_data);

  gtk_stack_set_visible_child (self->centerstack, GTK_WIDGET (self->centerbuttons_eos_box));

  self->player_state = STATE_PLAYER_EOS;
  change_playing_state (self, STATE_PLAYBACK_NULL, FALSE);
  // we're forcing reconfiguring by the state machine here
  _change_ui_state (self, STATE_UI_NULL, STATE_UI_PLAYER);

  cancel_timeout (self);
  reset_animation (self);
}

static void
on_close_description_clicked (GtkButton *,
                              gpointer   user_data)
{
  RvPlayer *self;

  g_assert (RV_IS_PLAYER (user_data));
  self = RV_PLAYER (user_data);

  change_ui_state (self, STATE_UI_PLAYER);
}

void
animation_cancel_event (RvPlayer *self)
{
  g_assert (RV_IS_PLAYER (self));

  reset_animation (self);
  cancel_timeout (self);
  if (self->playback_state == STATE_PLAYBACK_PLAYING) {
    start_timeout (self);
  }
}

void
overlay_on_motion_event (GtkEventControllerMotion* ctrl,
                         gdouble x, gdouble y,
                         RvPlayer *self)
{
  g_assert (RV_IS_PLAYER (self));

  animation_cancel_event (self);
}

void
overlay_on_pressed_event (GtkGestureClick* gstrclick,
                          gint n_press,
                          gdouble x,
                          gdouble y,
                          RvPlayer *self)
{
  g_assert (RV_IS_PLAYER (self));

  animation_cancel_event (self);
}

void
enable_ui_state_null (RvPlayer *self)
{
  disable_ui_state_player (self);
  disable_ui_state_description (self);
  // TODO: should be some sort of error symbol
  gtk_stack_set_visible_child(self->centerstack, self->startplaybutton);
  change_playing_state (self, STATE_PLAYBACK_NULL, FALSE);
}

// TODO: have a timeout before auto_hide animation, requires methods for abstraction
// TODO: do something if animations disabled
static void
rv_player_init (RvPlayer *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  self->ui_state = STATE_UI_NULL;
  self->player_state = STATE_PLAYER_READY;
  self->playback_state = STATE_PLAYBACK_NULL;

  g_signal_connect (self->frame, "notify::ready", on_ready_cb, self);
  g_signal_connect (self->frame, "play", on_state_play, self);
  g_signal_connect (self->frame, "pause", on_state_pause, self);
  g_signal_connect (self->frame, "eos", on_state_eos, self);

  g_signal_connect (self->startplaybutton, "clicked", on_playbutton_clicked, self);
  g_signal_connect (self->playbutton, "clicked", on_playbutton_clicked, self);
  g_signal_connect (self->pausebutton, "clicked", on_pausebutton_clicked, self);

  GtkExpression *src_expr = gtk_property_expression_new (RV_TYPE_PLAYER, NULL, "src");
  GtkExpression *title_expr = gtk_property_expression_new (RV_TYPE_VIDEO, src_expr, "title");
  gtk_expression_bind (title_expr, self->title_pill_title, "label", self);
  gtk_expression_bind (title_expr, self->description_pill_title, "label", self);
  GtkExpression *author_expr = gtk_property_expression_new (RV_TYPE_VIDEO, src_expr, "author");
  gtk_expression_bind (author_expr, self->title_pill_author, "label", self);
  gtk_expression_bind (author_expr, self->description_pill_author, "label", self);
  GtkExpression *description_expr = gtk_property_expression_new (RV_TYPE_VIDEO, src_expr, "description");
  gtk_expression_bind (description_expr, self->description_pill_description_text, "label", self);

  g_signal_connect (self->title_pill_description_button, "clicked", on_show_description_clicked, self);
  g_signal_connect (self->description_pill_close_btn, "clicked", on_close_description_clicked, self);

  self->auto_hide = adw_timed_animation_new (self, 1, 0, 500,
                                             adw_property_animation_target_new (self, "controls-opacity"));
  g_signal_connect (self->auto_hide, "done", on_animation_done, self);

  self->timeout_source_hide_animation_starter = NULL;

  GtkEventController *event_motion = gtk_event_controller_motion_new ();
  GtkEventController *event_touch = gtk_gesture_click_new();
  gtk_widget_add_controller (self->overlay, event_motion);
  gtk_widget_add_controller (self->overlay, event_touch);
  g_signal_connect (event_motion, "motion", overlay_on_motion_event, self);
  g_signal_connect (event_touch, "pressed", overlay_on_pressed_event, self);
  // FIXME: add more ways of activating the overlay (for touch input, ...)
}
