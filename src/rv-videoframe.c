/* rv-videoframe.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Receiver.VideoFrame"

#include "rv-videoframe.h"

#include "logic/rv-video.h"

#include <adwaita.h>
#include <gst/gst.h>

#include "rv-time.h"

struct _RvVideoFrame
{
  AdwBin parent_instance;

  gboolean ready;

  RvVideo *src;

  GBinding *binding;

  GstElement
   *sink_paintable,
   *sink_audio,
   *playbin;

  RvTime *time_adj;
  gchar *duration_str;
  gchar *position_str;
  GstState current_state;
  gulong slider_update_signal_id;
  guint hours_length;

  // Template
  GtkPicture *image;
};

G_DEFINE_FINAL_TYPE (RvVideoFrame, rv_videoframe, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_READY,
  PROP_SRC,
  PROP_PIPELINE,
  PROP_TIME,
  PROP_DURATION,
  PROP_POSITION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

enum {
  SIGNAL_EOS,
  SIGNAL_PLAY,
  SIGNAL_PAUSE,
  N_SIGNALS,
};

static gint signals [N_SIGNALS];

RvVideoFrame *
rv_videoframe_new (void)
{
  return g_object_new (RV_TYPE_VIDEOFRAME, NULL);
}

static void
rv_videoframe_dispose (GObject *object)
{
  RvVideoFrame *self = (RvVideoFrame *)object;

  gst_element_set_state (self->playbin, GST_STATE_NULL);

  g_clear_object (&self->src);

  g_clear_object (&self->playbin);
  g_clear_object (&self->sink_paintable);
  g_clear_object (&self->sink_audio);

  g_clear_object (&self->time_adj);

  G_OBJECT_CLASS (rv_videoframe_parent_class)->dispose (object);
}

static void
rv_videoframe_finalize (GObject *object)
{
  RvVideoFrame *self = (RvVideoFrame *)object;

  g_clear_pointer (&self->duration_str, g_free);
  g_clear_pointer (&self->position_str, g_free);

  G_OBJECT_CLASS (rv_videoframe_parent_class)->finalize (object);
}

static void
rv_videoframe_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  RvVideoFrame *self = RV_VIDEOFRAME (object);

  switch (prop_id)
    {
    case PROP_READY:
      g_value_set_boolean (value, self->ready);
      break;
    case PROP_PIPELINE:
      g_value_set_object (value, self->playbin);
      break;
    case PROP_SRC:
      g_value_set_object (value, self->src);
      break;
    case PROP_TIME:
      g_value_set_object (value, self->time_adj);
      break;
    case PROP_DURATION:
      g_value_set_string (value, self->duration_str);
      break;
    case PROP_POSITION:
      g_value_set_string (value, self->position_str);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
ready_cb (RvVideo *vid, gpointer, RvVideoFrame *self)
{
  g_assert (RV_IS_VIDEOFRAME (self));


}

gboolean
refresh_ui_cb (RvVideoFrame *self)
{
  gint64 current = -1;
  if (!RV_IS_VIDEOFRAME (self)) {
    return G_SOURCE_REMOVE;
  }

  if (self->current_state != GST_STATE_PLAYING && self->current_state != GST_STATE_PAUSED) {
    return G_SOURCE_REMOVE;
  }

  if (gst_element_query_position (self->playbin, GST_FORMAT_TIME, &current)) {
    /* Block the "value-changed" signal, so the slider_cb function is not called
     * (which would trigger a seek the user has not requested) */
    g_signal_handler_block (self->time_adj, self->slider_update_signal_id);
    /* Set the position of the slider to the current pipeline position, in MICROSECONDS */
    gtk_adjustment_set_value (GTK_ADJUSTMENT (self->time_adj), (gdouble)current / GST_MSECOND);
    /* Re-enable the signal */
    g_signal_handler_unblock (self->time_adj, self->slider_update_signal_id);
    g_clear_pointer (&self->position_str, g_free);

    guint hours = (guint) (current / (GST_SECOND * 60 * 60));
    guint mins =  (guint) ((current / (GST_SECOND * 60)) % 60);
    guint secs =  (guint) ((current / GST_SECOND) % 60);

    if (self->hours_length > 0) {
      gchar *hours_format_str =
        g_strdup_printf ("%s0%dd", "%", self->hours_length);
      gchar *hours_str =
        g_strdup_printf (hours_format_str, hours);

      self->position_str =
        g_strdup_printf("%s:%02u:%02u", hours_str, mins, secs);
      g_free (hours_str);
      g_free (hours_format_str);
    } else {
      if (hours == 0) {
        self->position_str = g_strdup_printf("%02u:%02u", mins, secs);
      } else {
        g_critical ("Although duration label doesn't include hours, we have at least an hour in position");
        self->position_str = g_strdup_printf("%d:%02u:%02u", hours, mins, secs);
      }
    }
    g_object_notify_by_pspec (self, properties[PROP_POSITION]);
  }

  return G_SOURCE_CONTINUE;
}


static void
rv_videoframe_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  RvVideoFrame *self = RV_VIDEOFRAME (object);

  switch (prop_id)
    {
    case PROP_SRC:
      RvVideo *oldsrc = self->src;
      RvVideo *newsrc = g_value_get_object (value);
      g_debug("Called rv-videoframe set src");
      if (newsrc == oldsrc){
        break;
      }
      g_clear_object (&self->src);
      g_object_ref (newsrc);
      self->src = newsrc;
      if (self->binding) {
        g_binding_unbind (self->binding);
      }
      self->ready = FALSE;
      gst_element_set_state (self->playbin, GST_STATE_READY);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_READY]);
      g_clear_pointer (&self->duration_str, g_free);
      self->duration_str = g_strdup ("");
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_DURATION]);
      g_clear_pointer (&self->position_str, g_free);
      self->position_str = g_strdup ("0:00");
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_POSITION]);
      if (self->src) {
        gchar *url = rv_video_get_video_uri (self->src);
        g_object_set (G_OBJECT (self->playbin), "uri", url, NULL);
        gst_element_set_state (self->playbin, GST_STATE_READY);
        g_free (url);

        self->ready = true;
        g_object_notify_by_pspec ( G_OBJECT (self), properties[PROP_READY]);
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_videoframe_class_init (RvVideoFrameClass *klass)
{
  GObjectClass *class_object = G_OBJECT_CLASS (klass);

  class_object->dispose = rv_videoframe_dispose;
  class_object->finalize = rv_videoframe_finalize;
  class_object->get_property = rv_videoframe_get_property;
  class_object->set_property = rv_videoframe_set_property;

  properties[PROP_READY] =
    g_param_spec_boolean ("ready", NULL, NULL,
                          FALSE,
                          G_PARAM_READABLE|
                          G_PARAM_EXPLICIT_NOTIFY|
                          G_PARAM_STATIC_STRINGS);

  properties[PROP_SRC] =
    g_param_spec_object ("src", NULL, NULL,
                         RV_TYPE_VIDEO,
                         G_PARAM_READWRITE|
                         G_PARAM_STATIC_STRINGS);

  properties[PROP_PIPELINE] =
    g_param_spec_object ("pipeline", NULL, NULL,
                         GST_TYPE_PIPELINE,
                         G_PARAM_READABLE|
                         G_PARAM_STATIC_STRINGS);

  properties[PROP_TIME] =
    g_param_spec_object ("time", NULL, NULL,
                         RV_TYPE_TIME,
                         G_PARAM_READABLE|
                         G_PARAM_EXPLICIT_NOTIFY|
                         G_PARAM_STATIC_STRINGS);

  properties[PROP_DURATION] =
    g_param_spec_string ("duration", NULL, NULL,
                         NULL,
                         G_PARAM_READABLE|
                         G_PARAM_EXPLICIT_NOTIFY|
                         G_PARAM_STATIC_STRINGS);

  properties[PROP_POSITION] =
    g_param_spec_string ("position", NULL, NULL,
                         NULL,
                         G_PARAM_READABLE|
                         G_PARAM_EXPLICIT_NOTIFY|
                         G_PARAM_STATIC_STRINGS);


  g_object_class_install_properties (class_object, N_PROPS, properties);

  signals [SIGNAL_EOS] =
    g_signal_new ("eos",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);

  signals [SIGNAL_PLAY] =
    g_signal_new ("play",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);

  signals [SIGNAL_PAUSE] =
    g_signal_new ("pause",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);

  GtkWidgetClass *class_widget = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (class_widget, "/app/drey/Receiver/ui/rv-videoframe.ui");
  gtk_widget_class_bind_template_child (class_widget, RvVideoFrame, image);
}

static void
error_cb (GstBus       *bus,
          GstMessage   *msg,
          RvVideoFrame *self)
{
  GError *err;
  gchar *debug_info;

  g_return_if_fail (RV_IS_VIDEOFRAME (self));

  /* Print error details on the screen */
  gst_message_parse_error (msg, &err, &debug_info);
  g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
  g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
  g_clear_error (&err);
  g_free (debug_info);

  /* Set the pipeline to READY (which stops playback) */
  gst_element_set_state (self->playbin, GST_STATE_READY);
}

static void
eos_cb (GstBus       *bus,
        GstMessage   *msg,
        RvVideoFrame *self)
{
  g_return_if_fail (RV_IS_VIDEOFRAME (self));

  g_warning ("End-Of-Stream reached.\n");

  g_signal_emit (self, signals[SIGNAL_EOS], 0);
}

/* This function is called when the pipeline changes states. We use it to
 * keep track of the current state. */
static void
state_changed_cb (GstBus       *bus,
                  GstMessage   *msg,
                  RvVideoFrame *self)
{
  g_assert (RV_IS_VIDEOFRAME (self));

  GstState old_state, new_state, pending_state;
  gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
  //g_warning ("State of element '%s' set from %s to %s (pending: %s)", GST_MESSAGE_SRC_NAME(msg), gst_element_state_get_name (old_state), gst_element_state_get_name (new_state), gst_element_state_get_name (pending_state));
  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (self->playbin)) {
    if (new_state == old_state) {
      return;
    }
    else {
      self->current_state = new_state;
      if (new_state == GST_STATE_READY) {
        self->ready = true;
        g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_READY]);
      }
      else if (new_state == GST_STATE_PLAYING) {
        g_timeout_add_seconds (0.5, (GSourceFunc)refresh_ui_cb, self);
        g_signal_emit (self, signals[SIGNAL_PLAY], 0);
      }
      else if (new_state == GST_STATE_PAUSED) {
        g_signal_emit (self, signals[SIGNAL_PAUSE], 0);
      }
    }
  }
}

static void
duration_changed_cb (GstBus *bus, GstMessage *msg, RvVideoFrame *self)
{
  g_assert (RV_IS_VIDEOFRAME (self));

  // TODO: test this code on a livestream
  guint64 duration;
  gboolean res;
  gchar *new_duration_str;
  guint new_hours_length;
  res = gst_element_query_duration (GST_ELEMENT (msg->src), GST_FORMAT_TIME, &duration);

  // from here: https://gstreamer.freedesktop.org/documentation/gstreamer/gstclock.html?gi-language=c#GST_TIME_FORMAT
  if (res == TRUE && duration != GST_CLOCK_TIME_NONE) {
    // don't show hours if we don't have to
    guint hours = (guint) (duration / (GST_SECOND * 60 * 60));
    guint mins =  (guint) ((duration / (GST_SECOND * 60)) % 60);
    guint secs =  (guint) ((duration / GST_SECOND) % 60);

    {
      gchar *hours_str =
        hours != 0 ?
          g_strdup_printf ("%u", hours) : NULL;

      new_hours_length =
        hours_str ?
          strlen (hours_str) : 0;

      new_duration_str = hours_str ?
        g_strdup_printf("%s:%02u:%02u", hours_str, mins, secs) :
        g_strdup_printf("%02u:%02u", mins, secs);

      g_free (hours_str);
    }

    gtk_adjustment_set_upper (GTK_ADJUSTMENT (self->time_adj), (gdouble) GST_TIME_AS_MSECONDS (duration));
    g_info ("duration = %"GST_TIME_FORMAT, GST_TIME_ARGS (duration));
  } else {
    new_duration_str = g_strdup ("");
    new_hours_length = 0;
    g_warning ("no duration, TODO: do smth with that info");
  }

  g_clear_pointer (&self->duration_str, g_free);
  self->hours_length = new_hours_length;
  self->duration_str = new_duration_str;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_DURATION]);
}

/* This function is called when the slider changes its position. We perform a seek to the
 * new position here. */
static void
slider_cb (GtkAdjustment *range,
           RvVideoFrame  *self)
{
  gdouble value = gtk_adjustment_get_value (GTK_ADJUSTMENT (self->time_adj));

  GstEvent *event =
    gst_event_new_seek (1.0, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH,
                        GST_SEEK_TYPE_SET,
                        (gint64)(value * GST_MSECOND),
                        GST_SEEK_TYPE_NONE,
                        -1);

  gst_element_call_async (self->playbin, gst_element_send_event, event, NULL);
}

static void
rv_videoframe_init (RvVideoFrame *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->time_adj = rv_time_new ();
  self->slider_update_signal_id = g_signal_connect (G_OBJECT (self->time_adj), "value-changed", G_CALLBACK (slider_cb), self);

  self->playbin = gst_element_factory_make ("playbin3", NULL);
  g_assert (GST_IS_ELEMENT (self->playbin));
  self->sink_paintable = gst_element_factory_make ("gtk4paintablesink", NULL);
  self->sink_audio = gst_element_factory_make ("autoaudiosink", NULL);

  g_object_set (self->playbin, "audio-sink", self->sink_audio, NULL);
  g_object_set (self->playbin, "video-sink", self->sink_paintable, NULL);

  g_object_bind_property (self->sink_paintable, "paintable",
                          self->image, "paintable",
                          G_BINDING_DEFAULT | G_BINDING_SYNC_CREATE);

  GstBus *bus = gst_element_get_bus (self->playbin);
  gst_bus_add_signal_watch (bus);

  g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)error_cb, self);
  g_signal_connect (G_OBJECT (bus), "message::eos", (GCallback)eos_cb, self);
  g_signal_connect (G_OBJECT (bus), "message::state-changed", (GCallback)state_changed_cb, self);
  /* g_signal_connect (G_OBJECT (bus), "message::application", (GCallback)application_cb, self); */
  g_signal_connect (G_OBJECT (bus), "message::duration-changed", (GCallback)duration_changed_cb, self);
  gst_object_unref (bus);
}

GObject*
rv_videoframe_get_pipeline (RvVideoFrame *self)
{
  g_assert (RV_IS_VIDEOFRAME (self));
  return G_OBJECT (self->playbin);
}
