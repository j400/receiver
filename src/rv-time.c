/* rv-time.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Receiver.Time"

#include "rv-time.h"

#include <gtk/gtk.h>

struct _RvTime
{
  GObject parent_instance;

  gboolean is_stream;
};

G_DEFINE_FINAL_TYPE (RvTime, rv_time, GTK_TYPE_ADJUSTMENT)

enum {
  PROP_0,
  PROP_IS_STREAM,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

RvTime *
rv_time_new (void)
{
  return g_object_new (RV_TYPE_TIME, NULL);
}

static void
rv_time_finalize (GObject *object)
{
  RvTime *self = (RvTime *)object;

  G_OBJECT_CLASS (rv_time_parent_class)->finalize (object);
}

static void
rv_time_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  RvTime *self = RV_TIME (object);

  switch (prop_id)
    {
    case PROP_IS_STREAM:
      g_value_set_boolean (value, self->is_stream);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_time_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  RvTime *self = RV_TIME (object);

  switch (prop_id)
    {
    case PROP_IS_STREAM:
      self->is_stream = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_time_class_init (RvTimeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rv_time_finalize;
  object_class->get_property = rv_time_get_property;
  object_class->set_property = rv_time_set_property;
}

static void
rv_time_init (RvTime *self)
{
  self->is_stream = FALSE;
}
