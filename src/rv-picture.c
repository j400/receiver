/*
 * Copyright © 2023 Jane Rachinger
 * Forked from GTK (/gtk/RvPicture.c) and retains it's license.
 * Copyright © 2018 Benjamin Otte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Benjamin Otte <otte@gnome.org> (in the original gtk/gtkpicture.c)
 */

#include "rv-picture.h"

#include <gtk/gtk.h>
#include <gdk/gdkpaintable.h>
#include <math.h>

enum
{
  PROP_0,
  PROP_PAINTABLE,
  PROP_ALTERNATIVE_TEXT,
  NUM_PROPERTIES
};

struct _RvPicture
{
  GtkWidget parent_instance;

  GdkPaintable *paintable;

  char *alternative_text;
};

G_DEFINE_FINAL_TYPE (RvPicture, rv_picture, GTK_TYPE_WIDGET)

static GParamSpec *properties[NUM_PROPERTIES] = { NULL, };

static void
rv_picture_snapshot (GtkWidget   *widget,
                       GtkSnapshot *snapshot)
{
  RvPicture *self = RV_PICTURE (widget);
  double ratio;
  int x, y, width, height;
  double w, h;

  if (self->paintable == NULL)
    return;


  /* if (self->content_fit == GTK_CONTENT_FIT_FILL || ratio == 0) */
  /*   { */
  /*     gdk_paintable_snapshot (self->paintable, snapshot, width, height); */
  /*   } */
  /* else */
  /*   { */
      width = gtk_widget_get_width (widget);
      height = gtk_widget_get_height (widget);
      double picture_ratio = (double) height / width;
      int paintable_width = gdk_paintable_get_intrinsic_width (self->paintable);
      int paintable_height = gdk_paintable_get_intrinsic_height (self->paintable);
      ratio = (double) paintable_height / paintable_width;

      /* if (self->content_fit == GTK_CONTENT_FIT_SCALE_DOWN && */
      /*     width >= paintable_width && height >= paintable_height) */
      /*   { */
      /*     w = paintable_width; */
      /*     h = paintable_height; */
      /*   } */
      /* else */
      if (ratio < picture_ratio)
        {
              w = height / ratio;
              h = height;
            }
      else
            {
              w = width;
              h = width * ratio;
            }

      x = floor(width - ceil (w)) / 2;
      y = (height - ceil (h)) / 2;

      gtk_snapshot_save (snapshot);
      gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (x, y));
      gdk_paintable_snapshot (self->paintable, snapshot, w, h);
      gtk_snapshot_restore (snapshot);
    /* } */
}

static GtkSizeRequestMode
rv_picture_get_request_mode (GtkWidget *widget)
{
  return GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH;
}

static void
rv_picture_measure (GtkWidget      *widget,
                     GtkOrientation  orientation,
                     int            for_size,
                     int           *minimum,
                     int           *natural,
                     int           *minimum_baseline,
                     int           *natural_baseline)
{
  RvPicture *self = RV_PICTURE (widget);
  double min_width, min_height, nat_width, nat_height;
  double default_size;

  /* for_size = 0 below is treated as -1, but we want to return zeros. */
  if (self->paintable == NULL || for_size == 0)
    {
      *minimum = 0;
      *natural = 0;
      return;
    }

  default_size = 10;

  min_width = min_height = 0;

  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
      gdk_paintable_compute_concrete_size (self->paintable,
                                           0,
                                           for_size < 0 ? 0 : for_size,
                                           default_size, default_size,
                                           &nat_width, &nat_height);

      *minimum = ceil (min_width);
      *natural = ceil (nat_width);
    }
  else
    {
      gdk_paintable_compute_concrete_size (self->paintable,
                                           0,
                                           for_size < 0 ? 0 : for_size,
                                           default_size, default_size,
                                           &nat_width, &nat_height);

      gdk_paintable_compute_concrete_size (self->paintable,
                                           for_size < 0 ? 0 : for_size,
                                           0,
                                           nat_width, nat_height,
                                           &nat_width, &nat_height);
      *minimum = ceil (nat_height);
      *natural = ceil (nat_height);
    }
}

static void
rv_picture_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  RvPicture *self = RV_PICTURE (object);

  switch (prop_id)
    {
    case PROP_PAINTABLE:
      rv_picture_set_paintable (self, g_value_get_object (value));
      break;

    case PROP_ALTERNATIVE_TEXT:
      rv_picture_set_alternative_text (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
rv_picture_get_property (GObject     *object,
                          guint        prop_id,
                          GValue      *value,
                          GParamSpec  *pspec)
{
  RvPicture *self = RV_PICTURE (object);

  switch (prop_id)
    {
    case PROP_PAINTABLE:
      g_value_set_object (value, self->paintable);
      break;

    case PROP_ALTERNATIVE_TEXT:
      g_value_set_string (value, self->alternative_text);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
rv_picture_paintable_invalidate_contents (GdkPaintable *paintable,
                                           RvPicture   *self)
{
  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static void
rv_picture_paintable_invalidate_size (GdkPaintable *paintable,
                                       RvPicture   *self)
{
  gtk_widget_queue_resize (GTK_WIDGET (self));
}

static void
rv_picture_clear_paintable (RvPicture *self)
{
  guint flags;

  if (self->paintable == NULL)
    return;

  flags = gdk_paintable_get_flags (self->paintable);

  if ((flags & GDK_PAINTABLE_STATIC_CONTENTS) == 0)
    g_signal_handlers_disconnect_by_func (self->paintable,
                                          rv_picture_paintable_invalidate_contents,
                                          self);

  if ((flags & GDK_PAINTABLE_STATIC_SIZE) == 0)
    g_signal_handlers_disconnect_by_func (self->paintable,
                                          rv_picture_paintable_invalidate_size,
                                          self);

  g_object_unref (self->paintable);
}

static void
rv_picture_dispose (GObject *object)
{
  RvPicture *self = RV_PICTURE (object);

  rv_picture_clear_paintable (self);

  g_clear_pointer (&self->alternative_text, g_free);

  G_OBJECT_CLASS (rv_picture_parent_class)->dispose (object);
};

static void
rv_picture_class_init (RvPictureClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  gobject_class->set_property = rv_picture_set_property;
  gobject_class->get_property = rv_picture_get_property;
  gobject_class->dispose = rv_picture_dispose;

  widget_class->snapshot = rv_picture_snapshot;
  widget_class->get_request_mode = rv_picture_get_request_mode;
  widget_class->measure = rv_picture_measure;

  /**
   * RvPicture:paintable: (attributes org.gtk.Property.get=rv_picture_get_paintable org.gtk.Property.set=rv_picture_set_paintable)
   *
   * The `GdkPaintable` to be displayed by this `RvPicture`.
   */
  properties[PROP_PAINTABLE] =
      g_param_spec_object ("paintable", NULL, NULL,
                           GDK_TYPE_PAINTABLE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /**
   * RvPicture:alternative-text: (attributes org.gtk.Property.get=rv_picture_get_alternative_text org.gtk.Property.set=rv_picture_set_alternative_text)
   *
   * The alternative textual description for the picture.
   */
  properties[PROP_ALTERNATIVE_TEXT] =
      g_param_spec_string ("alternative-text", NULL, NULL,
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (gobject_class, NUM_PROPERTIES, properties);

  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_IMG);
}

static void
rv_picture_init (RvPicture *self)
{
  gtk_widget_set_overflow (GTK_WIDGET (self), GTK_OVERFLOW_HIDDEN);
}

/**
 * rv_picture_new:
 *
 * Creates a new empty `RvPicture` widget.
 *
 * Returns: a newly created `RvPicture` widget.
 */
GtkWidget*
rv_picture_new (void)
{
  return g_object_new (GTK_TYPE_PICTURE, NULL);
}

/**
 * rv_picture_new_for_paintable:
 * @paintable: (nullable): a `GdkPaintable`
 *
 * Creates a new `RvPicture` displaying @paintable.
 *
 * The `RvPicture` will track changes to the @paintable and update
 * its size and contents in response to it.
 *
 * Returns: a new `RvPicture`
 */
GtkWidget*
rv_picture_new_for_paintable (GdkPaintable *paintable)
{
  g_return_val_if_fail (paintable == NULL || GDK_IS_PAINTABLE (paintable), NULL);

  return g_object_new (RV_TYPE_PICTURE,
                       "paintable", paintable,
                       NULL);
}

/**
 * rv_picture_set_paintable: (attributes org.gtk.Method.set_property=paintable)
 * @self: a `RvPicture`
 * @paintable: (nullable): a `GdkPaintable`
 *
 * Makes @self display the given @paintable.
 *
 * If @paintable is %NULL, nothing will be displayed.
 *
 * See [ctor@Gtk.Picture.new_for_paintable] for details.
 */
void
rv_picture_set_paintable (RvPicture   *self,
                           GdkPaintable *paintable)
{
  g_return_if_fail (RV_IS_PICTURE (self));
  g_return_if_fail (paintable == NULL || GDK_IS_PAINTABLE (paintable));

  if (self->paintable == paintable)
    return;

  g_object_freeze_notify (G_OBJECT (self));

  if (paintable)
    g_object_ref (paintable);

  rv_picture_clear_paintable (self);

  self->paintable = paintable;

  if (paintable)
    {
      const guint flags = gdk_paintable_get_flags (paintable);

      if ((flags & GDK_PAINTABLE_STATIC_CONTENTS) == 0)
        g_signal_connect (paintable,
                          "invalidate-contents",
                          G_CALLBACK (rv_picture_paintable_invalidate_contents),
                          self);

      if ((flags & GDK_PAINTABLE_STATIC_SIZE) == 0)
        g_signal_connect (paintable,
                          "invalidate-size",
                          G_CALLBACK (rv_picture_paintable_invalidate_size),
                          self);
    }

  gtk_widget_queue_resize (GTK_WIDGET (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_PAINTABLE]);

  g_object_thaw_notify (G_OBJECT (self));
}

/**
 * rv_picture_get_paintable: (attributes org.gtk.Method.get_property=paintable)
 * @self: a `RvPicture`
 *
 * Gets the `GdkPaintable` being displayed by the `RvPicture`.
 *
 * Returns: (nullable) (transfer none): the displayed paintable
 */
GdkPaintable *
rv_picture_get_paintable (RvPicture *self)
{
  g_return_val_if_fail (RV_IS_PICTURE (self), NULL);

  return self->paintable;
}

/**
 * rv_picture_set_alternative_text: (attributes org.gtk.Method.set_property=alternative-text)
 * @self: a `RvPicture`
 * @alternative_text: (nullable): a textual description of the contents
 *
 * Sets an alternative textual description for the picture contents.
 *
 * It is equivalent to the "alt" attribute for images on websites.
 *
 * This text will be made available to accessibility tools.
 *
 * If the picture cannot be described textually, set this property to %NULL.
 */
void
rv_picture_set_alternative_text (RvPicture *self,
                                  const char *alternative_text)
{
  g_return_if_fail (RV_IS_PICTURE (self));

  if (g_strcmp0 (self->alternative_text, alternative_text) == 0)
    return;

  g_free (self->alternative_text);
  self->alternative_text = g_strdup (alternative_text);

  gtk_accessible_update_property (GTK_ACCESSIBLE (self),
                                  GTK_ACCESSIBLE_PROPERTY_DESCRIPTION, alternative_text,
                                  -1);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ALTERNATIVE_TEXT]);
}

/**
 * rv_picture_get_alternative_text: (attributes org.gtk.Method.get_property=alternative-text)
 * @self: a `RvPicture`
 *
 * Gets the alternative textual description of the picture.
 *
 * The returned string will be %NULL if the picture cannot be described textually.
 *
 * Returns: (nullable) (transfer none): the alternative textual description of @self.
 */
const char *
rv_picture_get_alternative_text (RvPicture *self)
{
  g_return_val_if_fail (RV_IS_PICTURE (self), NULL);

  return self->alternative_text;
}

