/* rv-tabcontent.c
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-tab-content.h"

G_DEFINE_INTERFACE (RvTabContent, rv_tab_content, G_TYPE_OBJECT)

static void
rv_tab_content_default_init (RvTabContentInterface *iface)
{
  g_object_interface_install_property (iface,
                                       g_param_spec_string
                                       ("title", NULL, NULL,
                                        "",
                                        (G_PARAM_READWRITE|
                                         G_PARAM_STATIC_STRINGS)));
}
