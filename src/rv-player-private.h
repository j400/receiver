/* rv-player.c
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
#pragma once

#include "rv-player.h"

typedef enum _UIState {
  STATE_UI_NULL,
  STATE_UI_LOADING,
  STATE_UI_PLAYER,
  STATE_UI_DESCRIPTION,
} UIState;

typedef enum _PlayerState {
  STATE_PLAYER_READY,
  STATE_PLAYER_IN_PLAY,
  STATE_PLAYER_EOS,
} PlayerState;

typedef enum _PlaybackState {
  STATE_PLAYBACK_NULL,
  STATE_PLAYBACK_PAUSED,
  STATE_PLAYBACK_PLAYING,
} PlaybackState;

// UIState
void disable_ui_state_player (RvPlayer *self);
void enable_ui_state_player (RvPlayer *self);

void disable_ui_state_description (RvPlayer *self);
void enable_ui_state_description (RvPlayer *self);

void enable_ui_state_null (RvPlayer *self);

void switch_state_from_loading (RvPlayer *self, UIState new);
void switch_state_to_loading (RvPlayer *self);

void change_ui_state (RvPlayer *self, UIState new);
void _change_ui_state (RvPlayer *self, UIState old, UIState new);

// PlaybackState
void change_playing_state (RvPlayer *self, PlaybackState new, gboolean sync_gstreamer);

// Player Animation
void cancel_timeout (RvPlayer *self);
void on_animation_done (AdwAnimation *anim, RvPlayer *self);
void reset_animation (RvPlayer *self);
gboolean timeout_reached (RvPlayer *self);
void start_timeout (RvPlayer *self);
void restart_timeout (RvPlayer *self);
void animation_cancel_event (RvPlayer *self);
