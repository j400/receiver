/* rv-service.h
 *
 * Copyright 2022 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>
#include "rv-video.h"

G_BEGIN_DECLS

#define RV_TYPE_SERVICE (rv_service_get_type ())

G_DECLARE_INTERFACE (RvService, rv_service, RV, SERVICE, GObject)

struct _RvServiceInterface
{
  GTypeInterface parent;

  void (*new_video_async) (RvService *service,
                           const gchar *video_id,
                           gint priority, GCancellable *cancellable,
                           GAsyncReadyCallback callback, gpointer user_data);
  RvVideo* (*new_video_finish) (RvService *service,
                                GAsyncResult *result,
                                GError **error);
};

G_END_DECLS
