/* rv-thumbnail.c
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-thumbnail.h"

struct _RvThumbnail
{
  GObject parent_instance;

  /* TODO: Thumbnails should probably be fetched by the service backend
   * aka abstracting the download logic into the backend.
   * This move can probably be used to turn this into an interface.
   */
  gchar *url;
  guint width;
  guint height;
};

G_DEFINE_FINAL_TYPE (RvThumbnail, rv_thumbnail, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_URL,
  PROP_WIDTH,
  PROP_HEIGHT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

RvThumbnail *
rv_thumbnail_new (const gchar *url, guint width, guint height)
{
  return g_object_new (RV_TYPE_THUMBNAIL,
                       "url", url,
                       "width", width,
                       "height", height,
                       NULL);
}

static void
rv_thumbnail_finalize (GObject *object)
{
  RvThumbnail *self = (RvThumbnail *)object;

  g_clear_pointer (&self->url, g_free);

  G_OBJECT_CLASS (rv_thumbnail_parent_class)->finalize (object);
}

const gchar*
rv_thumbnail_get_url (RvThumbnail *self)
{
  g_assert (RV_IS_THUMBNAIL (self));
  return self->url;
}

static void
rv_thumbnail_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  RvThumbnail *self = RV_THUMBNAIL (object);

  switch (prop_id)
    {
    case PROP_URL:
      g_value_set_string (value, self->url);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_thumbnail_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  RvThumbnail *self = RV_THUMBNAIL (object);

  switch (prop_id)
    {
    case PROP_URL:
      self->url = g_value_dup_string (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    case PROP_WIDTH:
      self->width = g_value_get_uint (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    case PROP_HEIGHT:
      self->height = g_value_get_uint (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_thumbnail_class_init (RvThumbnailClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rv_thumbnail_finalize;
  object_class->get_property = rv_thumbnail_get_property;
  object_class->set_property = rv_thumbnail_set_property;

  properties [PROP_URL] =
    g_param_spec_string ("url", NULL, NULL,
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_URL, properties [PROP_URL]);

  properties [PROP_WIDTH] =
    g_param_spec_uint ("width", NULL, NULL,
                       0, G_MAXUINT, G_MAXUINT,
                       (G_PARAM_READWRITE |
                        G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_WIDTH, properties [PROP_WIDTH]);

  properties [PROP_HEIGHT] =
    g_param_spec_uint ("height", NULL, NULL,
                          0, G_MAXUINT, G_MAXUINT,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_HEIGHT, properties [PROP_HEIGHT]);
}

static void
rv_thumbnail_init (RvThumbnail *self)
{

}
