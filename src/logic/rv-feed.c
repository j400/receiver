/* rv-feed.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-feed.h"

#include "gio/gio.h"
#include "gtk/gtk.h"

#include "rv-item.h"

G_DEFINE_INTERFACE (RvFeed, rv_feed, G_TYPE_OBJECT);

static void
rv_feed_default_init (RvFeedInterface *iface)
{
  g_object_interface_install_property (iface,
                                       g_param_spec_boolean
                                       ("continuable", "continuable", "continuable",
                                        FALSE,
                                        G_PARAM_STATIC_STRINGS | G_PARAM_READABLE
                                       ));

}

static void
rv_feed_fetch_next_results_async (RvFeed *self,
                                  GCancellable *cancellable,
                                  GCallback callback,
                                  gpointer user_data)
{
  RvFeedInterface *iface;

  g_return_if_fail (RV_IS_FEED (self));
  /*FIXME:SAFE API*/

  iface = RV_FEED_GET_IFACE (self);
  g_return_if_fail (iface->fetch_next_results_async != NULL);
  iface->fetch_next_results_async(self, cancellable, callback, user_data);
}


static gboolean
rv_feed_is_continuable (RvFeed *self)
{
  g_return_val_if_fail (RV_IS_FEED (self), false);

  gboolean *ret = g_object_get_data (self, "continuable");
  g_return_val_if_fail (ret == NULL, false);
  return *ret;
}

