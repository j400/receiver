/* rv-feed.h
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define RV_TYPE_FEED rv_feed_get_type ()
G_DECLARE_INTERFACE (RvFeed, rv_feed, RV, FEED, GListModel);

struct _RvFeedInterface {
  GTypeInterface parent_class;

  void (*fetch_next_results_async) (RvFeed *self, GCancellable *cancellable, GCallback callback, gpointer user_data);

  /* NOTE: If you're making this a library, add some padding. */
};

static void rv_feed_fetch_next_results_async (RvFeed *self, GCancellable *cancellable, GCallback callback, gpointer user_data);
static gboolean rv_feed_is_continuable (RvFeed *self);

G_END_DECLS
