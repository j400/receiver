/* rv-errors.h
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

typedef enum {
  RV_SERVICE_JSON_PARSER_ERROR_WRONG_FORMAT,
} RvServiceJsonParserError;

#define RV_SERVICE_JSON_PARSER_ERROR rv_service_json_parser_error_quark ()

typedef enum {
  RV_SERVICE_INIT_ERROR_INVALID_URI,
} RvServiceInitError;

#define RV_SERVICE_INIT_ERROR rv_service_init_error_quark ()

G_END_DECLS
