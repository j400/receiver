/* rv-thumbnail-list.c
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-thumbnail-list.h"

#include <gio/gio.h>

struct _RvThumbnailList
{
  GObject parent_instance;

  GList *list;
};

static void g_list_model_init (GListModelInterface *iface);
G_DEFINE_FINAL_TYPE_WITH_CODE (RvThumbnailList, rv_thumbnail_list, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE(G_TYPE_LIST_MODEL, g_list_model_init))

enum {
  PROP_0,
  PROP_ITEM_TYPE,
  PROP_N_ITEMS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

RvThumbnailList *
rv_thumbnail_list_new (void)
{
  return g_object_new (RV_TYPE_THUMBNAIL_LIST, NULL);
}

static void
rv_thumbnail_list_finalize (GObject *object)
{
  RvThumbnailList *self = (RvThumbnailList *)object;

  G_OBJECT_CLASS (rv_thumbnail_list_parent_class)->finalize (object);
}

static void
rv_thumbnail_list_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  RvThumbnailList *self = RV_THUMBNAIL_LIST (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_thumbnail_list_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  RvThumbnailList *self = RV_THUMBNAIL_LIST (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_thumbnail_list_class_init (RvThumbnailListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rv_thumbnail_list_finalize;
  object_class->get_property = rv_thumbnail_list_get_property;
  object_class->set_property = rv_thumbnail_list_set_property;
}

static void
rv_thumbnail_list_init (RvThumbnailList *self)
{
  self->list = NULL;
  g_assert (G_IS_LIST_MODEL (self));
}

void
rv_thumbnail_list_append (RvThumbnailList *self, RvThumbnail *thumb)
{
  g_assert (RV_IS_THUMBNAIL_LIST (self));
  g_assert (RV_IS_THUMBNAIL (thumb));

  self->list = g_list_append (self->list, thumb);
}

GType
rv_thumbnail_list_get_item_type (GListModel *list)
{
  return RV_TYPE_THUMBNAIL;
}

guint
rv_thumbnail_list_get_n_items (GListModel *list)
{
  g_assert (RV_IS_THUMBNAIL_LIST (list));
  RvThumbnailList *self = (RvThumbnailList*)list;
  return g_list_length (self->list);
}

gpointer
rv_thumbnail_get_item (GListModel *list,
                       guint       position)
{
  g_assert (RV_IS_THUMBNAIL_LIST (list));
  RvThumbnailList *self = (RvThumbnailList*)list;
  return g_list_nth(self->list, position)->data;
}


static void
g_list_model_init (GListModelInterface *iface)
{
  iface->get_item_type = rv_thumbnail_list_get_item_type;
  iface->get_n_items = rv_thumbnail_list_get_n_items;
  iface->get_item = rv_thumbnail_get_item;
}
