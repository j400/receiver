/* rv-video.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Rv.Model.Video"

#include "rv-video.h"
#include "rv-feed.h"

G_DEFINE_INTERFACE (RvVideo, rv_video, G_TYPE_OBJECT)

static void
rv_video_default_init (RvVideoInterface *iface)
{
  g_object_interface_install_property (iface,
    g_param_spec_string ("video-id", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE|
                          G_PARAM_STATIC_STRINGS|
                          G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_string ("title", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE|
                          G_PARAM_STATIC_STRINGS|
                          G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_string ("author", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE|
                          G_PARAM_STATIC_STRINGS|
                          G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_string ("description", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE|
                          G_PARAM_STATIC_STRINGS|
                          G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_string ("video-uri", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE|
                          G_PARAM_STATIC_STRINGS|
                          G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_uint64 ("published", NULL, NULL,
                         0, G_MAXUINT64, 0,
                         (G_PARAM_READABLE|
                          G_PARAM_STATIC_STRINGS|
                          G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_int ("view-count", NULL, NULL,
                      0, G_MAXINT, 0,
                      (G_PARAM_READABLE|
                       G_PARAM_STATIC_STRINGS|
                       G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_int ("length", NULL, NULL,
                      0, G_MAXINT, 0,
                      (G_PARAM_READABLE|
                       G_PARAM_STATIC_STRINGS|
                       G_PARAM_EXPLICIT_NOTIFY)));

  g_object_interface_install_property (iface,
    g_param_spec_object ("recommended", NULL, NULL,
                         RV_TYPE_FEED,
                         (G_PARAM_READABLE|
                          G_PARAM_STATIC_STRINGS|
                          G_PARAM_EXPLICIT_NOTIFY)));

}

gchar*
rv_video_get_video_uri (RvVideo *self)
{
  g_assert (RV_IS_VIDEO (self));
  gchar *value;

  g_object_get (self, "video-uri", &value, NULL);

  return value;
}
