/* rv-thumbnail-list.h
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "rv-thumbnail.h"

G_BEGIN_DECLS

#define RV_TYPE_THUMBNAIL_LIST (rv_thumbnail_list_get_type())

G_DECLARE_FINAL_TYPE (RvThumbnailList, rv_thumbnail_list, RV, THUMBNAIL_LIST, GObject)

RvThumbnailList *rv_thumbnail_list_new (void);
void rv_thumbnail_list_append (RvThumbnailList *self, RvThumbnail *thumb);

G_END_DECLS
