/* rv-service.c
 *
 * Copyright 2022 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-service.h"

G_DEFINE_QUARK (rv-service-json-parser-error-quark, rv_service_json_parser_error)
G_DEFINE_QUARK (rv-service-init-error-quark, rv_service_init_error)

G_DEFINE_INTERFACE (RvService, rv_service, G_TYPE_OBJECT)

#define default_error(_name, _args, _ret) \
  static _ret \
  _name _args \
  { g_error ("NOT IMPLEMENTED: Service._name"); g_assert_not_reached (); }

default_error(default_new_video_async, (RvService*, gchar*, gint, GCancellable*, GAsyncReadyCallback, gpointer), void);
default_error(default_new_video_finish, (RvService*, GAsyncResult*, GError**), RvVideo*);

static void
rv_service_default_init (RvServiceInterface *iface)
{
  iface->new_video_async = default_new_video_async;
  iface->new_video_finish = default_new_video_finish;
}
