/* rv-invidious-private.h
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


#pragma once

/* Invidious API */


#define API "/api/v1/"
#define API_STATS              API "stats"
#define API_VIDEOS             API "videos/%s"
#define API_ANNOTATIONS        API "annotations/%s"
#define API_COMMENTS           API "comments/%s"
#define API_CAPTIONS           API "captions/%s"
#define API_TRENDING           API "trending/%s"
#define API_POPULAR            API "popular/%s"
#define API_CHANNELS           API "channels/%s"
#define API_CHANNELS_VIDEOS    API "channels/%s/videos"
#define API_CHANNELS_LATEST    API "channels/%s/latest"
#define API_CHANNELS_PLAYLISTS API "channels/%s/playlists"
#define API_CHANNELS_COMMENTS  API "channels/%s/comments"
#define API_CHANNELS_SEARCH    API "channels/search/%s"
#define API_SEARCH             API "search"
#define API_SEARCH_SUGGESTIONS API "search/suggestions"
#define API_PLAYLISTS          API "playlists/%s"
#define API_MIXES              API "mixes/%s"

typedef enum
{
  APIPARAM_LANG,
  APIPARAM_PRETTY,
  APIPARAM_FIELDS,
  N_APIPARAMETERS
} APIParameter;
static const char* APIParameters[N_APIPARAMETERS];

typedef enum
{
  APIPARAM_VIDEO_REGION,
  N_APIPARAMETERS_VIDEO
} APIParameterVideo;
static const char* APIParametersVideo[N_APIPARAMETERS_VIDEO];

/*
 * The default "archive" should be good enough for APIPARAM_ANNOTATIONS_SOURCE,
 * as invidious is already getting the annotations from archive.org.
 * The options "youtube" doesn't make sense as YouTube no longer provides them.
 */
typedef enum
{
  APIPARAM_ANNOTATIONS_SOURCE,
  N_APIPARAMETERS_ANNOTATIONS
} APIParameterAnnotations;
static const char* APIParametersAnnotations[N_APIPARAMETERS_ANNOTATIONS];

typedef enum
{
  APIPARAM_COMMENTS_SORTBY,
  APIPARAM_COMMENTS_SOURCE,
  APIPARAM_COMMENTS_CONTINUATION,
  N_APIPARAMETERS_COMMENTS
} APIParameterComments;
static const char* APIParametersComments[N_APIPARAMETERS_COMMENTS];

typedef enum
{
  APIPARAMOPT_COMMENTS_SORTBY_TOP,
  APIPARAMOPT_COMMENTS_SORTBY_NEW,
  N_APIPARAMETEROPTIONS_COMMENTS_SORTBY
} APIParameterOptionCommentsSortBy;
static const char* APIParameterOptionsCommentsSortBy[N_APIPARAMETEROPTIONS_COMMENTS_SORTBY];

typedef enum
{
  APIPARAMOPT_COMMENTS_SOURCE_YT,
  APIPARAMOPT_COMMENTS_SOURCE_REDDIT,
  N_APIPARAMETEROPTIONS_COMMENTS_SOURCE
} APIParameterOptionCommentsSource;
static const char* APIParameterOptionsCommentsSource[N_APIPARAMETEROPTIONS_COMMENTS_SOURCE];

typedef enum
{
  APIPARAM_CAPTIONS_LABEL,
  APIPARAM_CAPTIONS_LANG,
  APIPARAM_CAPTIONS_TLANG,
  APIPARAM_CPATIONS_REGION,
  N_APIPARAMETERS_CAPTIONS
} APIParameterCaptions;
static const char* APIParametersCaptions[N_APIPARAMETERS_CAPTIONS];

typedef enum
{
  APIPARAM_TRENDING_TYPE,
  APIPARAM_TRENDING_REGION,
  N_APIPARAMETERS_TRENGING
} APIParameterTrending;
static const char* APIParametersTrending[N_APIPARAMETERS_TRENGING];

typedef enum
{
  APIPARAMOPT_TRENDING_TYPE_MUSIC,
  APIPARAMOPT_TRENDING_TYPE_GAMING,
  APIPARAMOPT_TRENDING_TYPE_NEWS,
  APIPARAMOPT_TRENDING_TYPE_MOVIES,
  N_APIPARAMETEROPTIONS_TRENDING_TYPE
} APIParameterOptionTrendingType;
static const char* APIParameterOptionsTrendingType[N_APIPARAMETEROPTIONS_TRENDING_TYPE];

typedef enum
{
  APIPARAM_CHANNELS_SORTBY,
  N_APIPARAMETERS_CHANNELS
} APIParameterChannels;
static const char* APIParametersChannels[N_APIPARAMETERS_CHANNELS];

typedef enum
{
  APIPARAMOPT_CHANNELS_SORTBY_NEWEST,
  APIPARAMOPT_CHANNELS_SORTBY_OLDEST,
  APIPARAMOPT_CHANNELS_SORTBY_POPULAR,
  N_APIPARAMETEROPTIONS_CHANNEL_SORTBY
} APIParameterOptionChannelsSortBy;
static const char* APIParameterOptionsChannelsSortBy[N_APIPARAMETEROPTIONS_CHANNEL_SORTBY];

typedef enum
{
  APIPARAM_CHANNEL_VIDEOS_PAGE,
  APIPARAM_CHANNEL_VIDEOS_SORTBY,
  N_APIPARAMETERS_CHANNEL_VIDEOS
} APIParameterChannelVideos;
static const char* APIParametersChannelVideos[N_APIPARAMETERS_CHANNEL_VIDEOS];

typedef enum
{
  APIPARAM_CHANNEL_PLAYLISTS_CONTINUATION,
  APIPARAM_CHANNEL_PLAYLISTS_SORTBY,
  N_APIPARAMETERS_CHANNEL_PLAYLISTS
} APIParameterChannelPlaylists;
static const char* APIParametersChannelPlaylists[N_APIPARAMETERS_CHANNEL_PLAYLISTS];

typedef enum
{
  APIPARAMOPT_CHANNEL_PLAYLISTS_SORTBY_NEWEST,
  APIPARAMOPT_CHANNEL_PLAYLISTS_SORTBY_OLDEST,
  APIPARAMOPT_CHANNEL_PLAYLISTS_SORTBY_LAST,
  N_APIPARAMETEROPTIONS_CHANNEL_PLAYLISTS_SORTBY
} APIParameterOptionChannelPlaylistsSortBy;
static const char* APIParameterOptionsChannelPlaylistsSortBy[N_APIPARAMETEROPTIONS_CHANNEL_PLAYLISTS_SORTBY];

typedef enum
{
  APIPARAM_CHANNEL_COMMENTS_CONTINUATION,
  N_APIPARAMETERS_CHANNEL_COMMENTS
} APIParameterChannelComments;
static const char* APIParametersChannelComments[N_APIPARAMETERS_CHANNEL_COMMENTS];

typedef enum
{
  APIPARAM_CHANNEL_SEARCH_QUERY,
  APIPARAM_CHANNEL_SEARCH_PAGE,
  N_APIPARAMETERS_CHANNEL_SEARCH
} APIParameterChannelSearch;
static const char* APIParametersChannelSearch[N_APIPARAMETERS_CHANNEL_SEARCH];

typedef enum
{
  APIPARAM_SEARCH_SUGGESTIONS_QUERY,
  N_APIPARAMETERS_SEARCH_SUGGESTIONS
} APIParameterSearchSuggestions;
static const char* APIParametersSearchSuggestions[N_APIPARAMETERS_SEARCH_SUGGESTIONS];

typedef enum
{
  APIPARAM_SEARCH_QUERY,
  APIPARAM_SEARCH_PAGE,
  APIPARAM_SEARCH_SORTBY,
  APIPARAM_SEARCH_DATE,
  APIPARAM_SEARCH_DURATION,
  APIPARAM_SEARCH_TYPE,
  APIPARAM_SEARCH_FEATURES,
  APIPARAM_SEARCH_REGION,
  N_APIPARAMETERS_SEARCH
} APIParameterSearch;
static const char* APIParametersSearch[N_APIPARAMETERS_SEARCH];

typedef enum
{
  APIPARAMOPT_SEARCH_SORTBY_RELEVANCE,
  APIPARAMOPT_SEARCH_SORTBY_RATING,
  APIPARAMOPT_SEARCH_SORTBY_UPLOAD_DATE,
  APIPARAMOPT_SEARCH_SORTBY_VIEW_COUNT,
  N_APIPARAMETEROPTIONS_SEARCH_SORTBY,
} APIParameterOptionSearchSortBy;
static const char* APIParameterOptionsSearchSortBy[N_APIPARAMETEROPTIONS_SEARCH_SORTBY];

typedef enum
{
  APIPARAMOPT_SEARCH_DATE_HOUR,
  APIPARAMOPT_SEARCH_DATE_TODAY,
  APIPARAMOPT_SEARCH_DATE_WEEK,
  APIPARAMOPT_SEARCH_DATE_MONTH,
  APIPARAMOPT_SEARCH_DATE_YEAR,
  N_APIPARAMETEROPTIONS_SEARCH_DATE
} APIParameterOptionSearchDate;
static const char* APIParameterOptionsSearchDate[N_APIPARAMETEROPTIONS_SEARCH_DATE];

typedef enum {
  APIPARAMOPT_SEARCH_DURATION_SHORT,
  APIPARAMOPT_SEARCH_DURATION_LONG,
  N_APIPARAMETEROPTIONS_SEARCH_DURATION
} APIParameterOptionSearchDuration;
static const char* APIParameterOptionsSearchDuration[N_APIPARAMETEROPTIONS_SEARCH_DURATION];

typedef enum
{
  APIPARAMOPT_SEARCH_TYPE_VIDEO,
  APIPARAMOPT_SEARCH_TYPE_PLAYLISTS,
  APIPARAMOPT_SEARCH_TYPE_CHANNEL,
  APIPARAMOPT_SEARCH_TYPE_ALL,
  N_APIPARAMETEROPTIONS_SEARCH_TYPE,
} APIParameterOptionSearchType;
static const char* APIParameterOptionsSearchType[N_APIPARAMETEROPTIONS_SEARCH_TYPE];

typedef enum
{
  APIPARAMOPT_SEARCH_FEATURES_HD,
  APIPARAMOPT_SEARCH_FEATURES_SUBTITLES,
  APIPARAMOPT_SEARCH_FEATURES_CREATIVE_COMMONS,
  APIPARAMOPT_SEARCH_FEATURES_3D,
  APIPARAMOPT_SEARCH_FEATURES_LIVE,
  APIPARAMOPT_SEARCH_FEATURES_PURCHASED,
  APIPARAMOPT_SEARCH_FEATURES_4K,
  APIPARAMOPT_SEARCH_FEATURES_360,
  APIPARAMOPT_SEARCH_FEATURES_LOCATION,
  APIPARAMOPT_SEARCH_FEATURES_HDR,
  N_APIPARAMETEROPTIONS_SEARCH_FEATURES,
} APIParameterOptionSearchFeatures;
static const char* APIParameterOptionsSearchFeatures[N_APIPARAMETEROPTIONS_SEARCH_FEATURES];

typedef enum
{
  APIPARAM_PLAYLISTS_PAGE,
  N_APIPARAMETERS_PLAYLISTS
} APIParameterPlaylists;
static const char* APIParametersPlaylists[N_APIPARAMETERS_PLAYLISTS];
