/* rv-invidious-private.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "invidious-api.h"

static const char* APIParameters[N_APIPARAMETERS] = {
  [APIPARAM_LANG]   = "hl",
  [APIPARAM_PRETTY] = "pretty",
  [APIPARAM_FIELDS] = "fields"
};

static const char* APIParametersVideo[N_APIPARAMETERS_VIDEO] = {
  [APIPARAM_VIDEO_REGION] = "region", // ISO3166 country code
};

static const char* APIParametersAnnotations[N_APIPARAMETERS_ANNOTATIONS] = {
  [APIPARAM_ANNOTATIONS_SOURCE] = "source"
};

static const char* APIParametersComments[N_APIPARAMETERS_COMMENTS] = {
  [APIPARAM_COMMENTS_SORTBY]       = "sort_by",
  [APIPARAM_COMMENTS_SOURCE]       = "source",
  [APIPARAM_COMMENTS_CONTINUATION] = "continuations"
};

static const char* APIParameterOptionsCommentsSortBy[N_APIPARAMETEROPTIONS_COMMENTS_SORTBY] = {
  [APIPARAMOPT_COMMENTS_SORTBY_TOP] = "top",
  [APIPARAMOPT_COMMENTS_SORTBY_NEW] = "new",
};

static const char* APIParameterOptionsCommentsSource[N_APIPARAMETEROPTIONS_COMMENTS_SOURCE] = {
  [APIPARAMOPT_COMMENTS_SOURCE_YT]     = "youtube",
  [APIPARAMOPT_COMMENTS_SOURCE_REDDIT] = "reddit",
};

static const char* APIParametersCaptions[N_APIPARAMETERS_CAPTIONS] = {
  [APIPARAM_CAPTIONS_LABEL]  = "label",
  [APIPARAM_CAPTIONS_LANG]   = "lang",
  [APIPARAM_CAPTIONS_TLANG]  = "tlang",
  [APIPARAM_CPATIONS_REGION] = "region",
};

static const char* APIParametersTrending[N_APIPARAMETERS_TRENGING] = {
  [APIPARAM_TRENDING_TYPE] = "type",
  [APIPARAM_TRENDING_REGION] = "region",
};

static const char* APIParameterOptionsTrendingType[N_APIPARAMETEROPTIONS_TRENDING_TYPE] = {
  [APIPARAMOPT_TRENDING_TYPE_MUSIC]  = "music",
  [APIPARAMOPT_TRENDING_TYPE_GAMING] = "gaming",
  [APIPARAMOPT_TRENDING_TYPE_NEWS]   = "news",
  [APIPARAMOPT_TRENDING_TYPE_MOVIES] = "movies",
};

static const char* APIParametersChannels[N_APIPARAMETERS_CHANNELS] = {
  [APIPARAM_CHANNELS_SORTBY] = "sort_by",
};

static const char* APIParameterOptionsChannelsSortBy[N_APIPARAMETEROPTIONS_CHANNEL_SORTBY] = {
  [APIPARAMOPT_CHANNELS_SORTBY_NEWEST]  = "newest",
  [APIPARAMOPT_CHANNELS_SORTBY_OLDEST]  = "oldest",
  [APIPARAMOPT_CHANNELS_SORTBY_POPULAR] = "popular",
};

static const char* APIParametersChannelVideos[N_APIPARAMETERS_CHANNEL_VIDEOS] = {
  [APIPARAM_CHANNEL_VIDEOS_PAGE]   = "page",
  [APIPARAM_CHANNEL_VIDEOS_SORTBY] = "sort_by",
};

static const char* APIParametersChannelPlaylists[N_APIPARAMETERS_CHANNEL_PLAYLISTS] = {
  [APIPARAM_CHANNEL_PLAYLISTS_CONTINUATION] = "continuation",
  [APIPARAM_CHANNEL_PLAYLISTS_SORTBY]       = "sort_by",
};

static const char* APIParameterOptionsChannelPlaylistsSortBy[N_APIPARAMETEROPTIONS_CHANNEL_PLAYLISTS_SORTBY] = {
  [APIPARAMOPT_CHANNEL_PLAYLISTS_SORTBY_NEWEST]  = "newest",
  [APIPARAMOPT_CHANNEL_PLAYLISTS_SORTBY_OLDEST]  = "oldest",
  [APIPARAMOPT_CHANNEL_PLAYLISTS_SORTBY_LAST]    = "last",
};

static const char* APIParametersChannelComments[N_APIPARAMETERS_CHANNEL_COMMENTS] = {
  [APIPARAM_CHANNEL_COMMENTS_CONTINUATION] = "continuation",
};

static const char* APIParametersChannelSearch[N_APIPARAMETERS_CHANNEL_SEARCH] = {
  [APIPARAM_CHANNEL_SEARCH_QUERY] = "q",
  [APIPARAM_CHANNEL_SEARCH_PAGE]  = "page",
};

static const char* APIParametersSearchSuggestions[N_APIPARAMETERS_SEARCH_SUGGESTIONS] = {
  [APIPARAM_SEARCH_SUGGESTIONS_QUERY] = "q",
};

static const char* APIParametersSearch[N_APIPARAMETERS_SEARCH] = {
  [APIPARAM_SEARCH_QUERY]    = "q",
  [APIPARAM_SEARCH_PAGE]     = "page",
  [APIPARAM_SEARCH_SORTBY]   = "sort_by",
  [APIPARAM_SEARCH_DATE]     = "date",
  [APIPARAM_SEARCH_DURATION] = "duration",
  [APIPARAM_SEARCH_TYPE]     = "type",
  [APIPARAM_SEARCH_FEATURES] = "features",
  [APIPARAM_SEARCH_REGION]   = "region",
};

static const char* APIParameterOptionsSearchSortBy[N_APIPARAMETEROPTIONS_SEARCH_SORTBY] = {
  [APIPARAMOPT_SEARCH_SORTBY_RELEVANCE]   = "relevance",
  [APIPARAMOPT_SEARCH_SORTBY_RATING]      = "rating",
  [APIPARAMOPT_SEARCH_SORTBY_UPLOAD_DATE] = "upload_date",
  [APIPARAMOPT_SEARCH_SORTBY_VIEW_COUNT]  = "view_count",
};

static const char* APIParameterOptionsSearchDate[N_APIPARAMETEROPTIONS_SEARCH_DATE] = {
  [APIPARAMOPT_SEARCH_DATE_HOUR]  = "hour",
  [APIPARAMOPT_SEARCH_DATE_TODAY] = "today",
  [APIPARAMOPT_SEARCH_DATE_WEEK]  = "week",
  [APIPARAMOPT_SEARCH_DATE_MONTH] = "month",
  [APIPARAMOPT_SEARCH_DATE_YEAR]  = "year",
};

static const char* APIParameterOptionsSearchDuration[N_APIPARAMETEROPTIONS_SEARCH_DURATION] = {
  [APIPARAMOPT_SEARCH_DURATION_SHORT] = "short",
  [APIPARAMOPT_SEARCH_DURATION_LONG]  = "long",
};

static const char* APIParameterOptionsSearchType[N_APIPARAMETEROPTIONS_SEARCH_TYPE] = {
  [APIPARAMOPT_SEARCH_TYPE_VIDEO]     = "video",
  [APIPARAMOPT_SEARCH_TYPE_PLAYLISTS] = "playlists",
  [APIPARAMOPT_SEARCH_TYPE_CHANNEL]   = "channel",
  [APIPARAMOPT_SEARCH_TYPE_ALL]       = "all",
};

static const char* APIParameterOptionsSearchFeatures[N_APIPARAMETEROPTIONS_SEARCH_FEATURES] = {
  [APIPARAMOPT_SEARCH_FEATURES_HD]               = "hd",
  [APIPARAMOPT_SEARCH_FEATURES_SUBTITLES]        = "subtitles",
  [APIPARAMOPT_SEARCH_FEATURES_CREATIVE_COMMONS] = "creative_commons",
  [APIPARAMOPT_SEARCH_FEATURES_3D]               = "3d",
  [APIPARAMOPT_SEARCH_FEATURES_LIVE]             = "live",
  [APIPARAMOPT_SEARCH_FEATURES_PURCHASED]        = "purchased",
  [APIPARAMOPT_SEARCH_FEATURES_4K]               = "4k",
  [APIPARAMOPT_SEARCH_FEATURES_360]              = "360",
  [APIPARAMOPT_SEARCH_FEATURES_LOCATION]         = "location",
  [APIPARAMOPT_SEARCH_FEATURES_HDR]              = "hdr",
};

static const char* APIParametersPlaylists[N_APIPARAMETERS_PLAYLISTS] = {
  [APIPARAM_PLAYLISTS_PAGE] = "page",
};
