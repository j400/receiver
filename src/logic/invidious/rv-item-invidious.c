/* rv-item-invidious.c
 *
 * Copyright 2022-2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Rv.Service.Invidious.Item"

#include <logic/rv-item.h>
#include <logic/rv-thumbnail-list.h>

#include "rv-item-invidious.h"

#include <json-glib/json-glib.h>


struct _RvItemInvidious
{
  GObject parent_instance;

  RvThumbnailList *thumbs;
  gchar *uri;
  gchar *title;
  gchar *creator;
};

enum {
  PROP_ITEM_URI = 1,
  PROP_ITEM_TITLE,
  PROP_ITEM_CREATOR,
  PROP_ITEM_THUMBNAIL_LIST,
  N_ITEM_PROPS
};

static void rv_item_invidious_init_iface (RvItemInterface*);
G_DEFINE_FINAL_TYPE_WITH_CODE (RvItemInvidious, rv_item_invidious, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE(RV_TYPE_ITEM, rv_item_invidious_init_iface))

static void
rv_item_invidious_finalize (GObject *object)
{
  RvItemInvidious *self = (RvItemInvidious *)object;

  g_clear_pointer (&self->uri, g_free);
  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->creator, g_free);

  G_OBJECT_CLASS (rv_item_invidious_parent_class)->finalize (object);
}

static void
rv_item_invidious_dispose (GObject *object)
{
  RvItemInvidious *self = (RvItemInvidious *)object;

  g_clear_object (&self->thumbs);

  G_OBJECT_CLASS (rv_item_invidious_parent_class)->dispose (object);
}

static void
rv_item_invidious_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  RvItemInvidious *self = RV_ITEM_INVIDIOUS (object);

  switch (prop_id)
    {
    case PROP_ITEM_URI:
      g_value_set_string (value, self->uri);
      break;
    case PROP_ITEM_TITLE:
      g_value_set_string (value, self->title);
      break;
    case PROP_ITEM_CREATOR:
      g_value_set_string (value, self->creator);
      break;
    case PROP_ITEM_THUMBNAIL_LIST:
      g_value_set_object (value, self->thumbs);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_item_invidious_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  RvItemInvidious *self = RV_ITEM_INVIDIOUS (object);

  switch (prop_id)
    {
    case PROP_ITEM_URI:
      self->uri = g_value_dup_string (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    case PROP_ITEM_TITLE:
      self->title = g_value_dup_string (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    case PROP_ITEM_CREATOR:
      self->creator = g_value_dup_string (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    case PROP_ITEM_THUMBNAIL_LIST:
      self->thumbs = g_value_dup_object (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      return;
    }
}

static void
rv_item_invidious_class_init (RvItemInvidiousClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rv_item_invidious_finalize;
  object_class->get_property = rv_item_invidious_get_property;
  object_class->set_property = rv_item_invidious_set_property;

  g_object_class_override_property (object_class, PROP_ITEM_URI, "uri");
  g_object_class_override_property (object_class, PROP_ITEM_TITLE, "title");
  g_object_class_override_property (object_class, PROP_ITEM_CREATOR, "creator");
  g_object_class_override_property (object_class, PROP_ITEM_THUMBNAIL_LIST, "thumbnails");
}

static void
rv_item_invidious_init_iface (RvItemInterface *)
{}

static void
rv_item_invidious_init (RvItemInvidious *)
{}

static void
_json_parse_videoThumbnail (RvThumbnailList *list, JsonNode *node)
{
  gchar *url;
  guint width;
  guint height;
  JsonObject *object;

  g_assert (JSON_NODE_HOLDS_OBJECT (node));
  object = json_node_get_object (node);

  g_assert (json_object_has_member (object, "url"));
  g_assert (json_object_has_member (object, "width"));
  g_assert (json_object_has_member (object, "height"));

  // The only proper video thumbnail appears to be mqdefaults
  const gchar *str = json_object_get_string_member (object, "url");
  if (!g_str_has_suffix (str, "/mqdefault.jpg")) {
    return;
  }

  url = g_strdup (json_object_get_string_member (object, "url"));
  width = json_object_get_int_member (object, "width");
  height = json_object_get_int_member (object, "height");

  rv_thumbnail_list_append (list, rv_thumbnail_new (url, width, height));
}

RvItemInvidious*
rv_item_invidious_new_from_json_object (JsonObject *obj)
{
  // FIXME: memory leaks on failure
  RvItemInvidious *self = NULL;

  const gchar *type;

  type = json_object_get_string_member_with_default (obj, "type", NULL);

  if (g_strcmp0 (type, "video")) {
    gchar *vid;
    gchar *uri;
    gchar *title;
    gchar *creator;
    JsonArray *thumbs_json;
    GList *thumb_list = NULL;
    RvThumbnailList *thumbs;

    thumbs = rv_thumbnail_list_new ();

    vid = json_object_get_string_member_with_default (obj, "videoId", NULL);
    g_return_val_if_fail (vid != NULL, NULL);

    uri = g_strdup_printf("receiver:service//youtube/video/%s", vid, NULL);
    g_return_val_if_fail (uri != NULL, NULL);

    title = json_object_get_string_member_with_default (obj, "title", NULL);
    g_return_val_if_fail (title != NULL, NULL);

    // abstract the author and authorId into seperate model
    creator = json_object_get_string_member_with_default (obj, "author", NULL);
    g_return_val_if_fail (creator != NULL, NULL);

    if (json_object_has_member (obj, "videoThumbnails")) {
      thumbs_json = json_object_get_array_member (obj, "videoThumbnails");
      thumb_list = json_array_get_elements (thumbs_json);
      while (thumb_list) {
        _json_parse_videoThumbnail (thumbs, thumb_list->data);
        thumb_list = thumb_list->next;
      }
    }

    self = g_object_new (RV_TYPE_ITEM_INVIDIOUS,
                         "uri", uri,
                         "title", title,
                         "creator", creator,
                         "thumbnails", thumbs,
                         NULL);
    g_free (vid);
    g_free (uri);
    g_free (title);
    g_free (creator);
  } else if (g_strcmp0 (type, "playlist")){

  }

  g_free (type);

  return self;
}
