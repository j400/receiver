/* rv-video-invidious.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-video-invidious.h"

#include <logic/rv-errors.h>
#include <logic/rv-video.h>

#include "invidious-api.h"
#include "rv-feed-invidious.h"

struct _RvVideoInvidious
{
  GObject parent_instance;

  gchar *video_id;
  gchar *title;
  gchar *author;
  gchar *description;
  guint64 published;
  gint view_count;
  gint length;
  gchar *video_uri;

  RvFeedInvidious *recommended;
};

static void rv_video_invidious_init_iface (RvVideoInterface*);
G_DEFINE_FINAL_TYPE_WITH_CODE (RvVideoInvidious, rv_video_invidious, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE(RV_TYPE_VIDEO, rv_video_invidious_init_iface))

#define JSON_OBJECT_GET_STRING_AND_SET_TO_SELF_OR_RUN_CODE(json_object_object, json_member, self_member_prop, self_member_prop_name, fail_code) \
      if (json_object_has_member (json_object_object, json_member)) { \
        self->self_member_prop = g_strdup (json_object_get_string_member (json_object_object, json_member)); \
        g_object_notify (self, self_member_prop_name); \
      } else { \
        fail_code ; \
      }
#define JSON_OBJECT_GET_STRING_AND_SET_TO_SELF_OR_ERROR(json_object_object, json_member, self_member_prop, self_member_prop_name, free_code) \
    JSON_OBJECT_GET_STRING_AND_SET_TO_SELF_OR_RUN_CODE(json_object_object, json_member, self_member_prop, self_member_prop_name, \
      g_task_return_new_error (task, RV_SERVICE_JSON_PARSER_ERROR, RV_SERVICE_JSON_PARSER_ERROR_WRONG_FORMAT, "JSON-Object lacks the string member \"%s\", this JSON is invalid", json_member); \
      free_code ; \
      return; \
    )
#define RV_VIDEO_INVIDIOUS_PARSE_STRING(json_member, self_member_prop, self_member_prop_name) \
    JSON_OBJECT_GET_STRING_AND_SET_TO_SELF_OR_ERROR(obj, json_member, self_member_prop, self_member_prop_name, g_object_unref (self); g_object_unref (parser); g_object_unref (service); )
#define RV_VIDEO_INVIDIOUS_PARSE_OPTIONAL_STRING(json_member, self_member_prop, self_member_prop_name) \
    JSON_OBJECT_GET_STRING_AND_SET_TO_SELF_OR_RUN_CODE(obj, json_member, self_member_prop, self_member_prop_name, \
      self->self_member_prop = NULL; \
      g_object_notify (self, self_member_prop_name); \
    )


enum {
  PROP_VIDEO_ID = 1,
  PROP_TITLE,
  PROP_AUTHOR,
  PROP_DESCRIPTION,
  PROP_PUBLISHED,
  PROP_VIEWCOUNT,
  PROP_LENGTH,
  PROP_VIDEO_URI,
  PROP_RECOMMENDEDLIST,

  N_PROPS
};


RvVideoInvidious*
rv_video_invidious_new ()
{
  return g_object_new (RV_TYPE_VIDEO_INVIDIOUS, NULL);
}

static void
rv_video_invidious_dispose (GObject *object)
{
  RvVideoInvidious *self = (RvVideoInvidious *)object;

  g_clear_object (&self->recommended);

  G_OBJECT_CLASS (rv_video_invidious_parent_class)->dispose (object);
}

static void
rv_video_invidious_finalize (GObject *object)
{
  RvVideoInvidious *self = (RvVideoInvidious *)object;

  g_clear_pointer (&self->video_id, g_free);
  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->author, g_free);
  g_clear_pointer (&self->description, g_free);
  g_clear_pointer (&self->video_uri, g_free);

  G_OBJECT_CLASS (rv_video_invidious_parent_class)->finalize (object);
}

static void
rv_video_invidious_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  RvVideoInvidious *self = RV_VIDEO_INVIDIOUS (object);

  switch (prop_id)
    {
      case PROP_VIDEO_ID:
        g_value_set_string (value, self->video_id);
        break;

      case PROP_TITLE:
        g_value_set_string (value, self->title);
        break;

      case PROP_AUTHOR:
        g_value_set_string (value, self->author);
        break;

      case PROP_DESCRIPTION:
        g_value_set_string (value, self->description);
        break;

      case PROP_PUBLISHED:
        g_value_set_uint64 (value, self->published);
        break;

      case PROP_VIEWCOUNT:
        g_value_set_int (value, self->view_count);
        break;

      case PROP_LENGTH:
        g_value_set_int (value, self->length);
        break;

      case PROP_VIDEO_URI:
        g_value_set_string (value, self->video_uri);
        break;

      case PROP_RECOMMENDEDLIST:
        g_value_set_object (value, self->recommended);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rv_video_invidious_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  RvVideoInvidious *self = RV_VIDEO_INVIDIOUS (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_video_invidious_class_init (RvVideoInvidiousClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rv_video_invidious_finalize;
  object_class->dispose = rv_video_invidious_dispose;
  object_class->get_property = rv_video_invidious_get_property;
  object_class->set_property = rv_video_invidious_set_property;

  g_object_class_override_property (object_class, PROP_VIDEO_ID, "video-id");
  g_object_class_override_property (object_class, PROP_TITLE, "title");
  g_object_class_override_property (object_class, PROP_AUTHOR, "author");
  g_object_class_override_property (object_class, PROP_DESCRIPTION, "description");
  g_object_class_override_property (object_class, PROP_PUBLISHED, "published");
  g_object_class_override_property (object_class, PROP_VIEWCOUNT, "view-count");
  g_object_class_override_property (object_class, PROP_LENGTH, "length");
  g_object_class_override_property (object_class, PROP_VIDEO_URI, "video-uri");
  g_object_class_override_property (object_class, PROP_RECOMMENDEDLIST, "recommended");
}

static void
rv_video_invidious_init_iface (RvVideoInterface*)
{}

static void
rv_video_invidious_init (RvVideoInvidious *self)
{
  g_assert (g_type_is_a (RV_TYPE_VIDEO_INVIDIOUS, RV_TYPE_VIDEO));
}

static void
video_parser (GTask *task,
              RvServiceInvidious *service,
              const gchar *uri,
              GCancellable* cancellable)
{
  GError *error = NULL;
  JsonParser *parser;
  JsonNode *root;
  JsonNodeType root_type;
  JsonObject *obj;
  JsonArray *recommened_arr;
  RvVideoInvidious *self;

  parser = rv_invidious_api (service, uri, cancellable, &error);
  if (error != NULL) {
    g_task_return_error (task, error);
    g_object_unref (service);
    return;
  }

  root = json_parser_get_root(parser);
  root_type = json_node_get_node_type (root);

  if (root_type != JSON_NODE_OBJECT) {
    g_error ("JSON: Wrong format, root not an object");
    g_task_return_new_error (task, RV_SERVICE_JSON_PARSER_ERROR, RV_SERVICE_JSON_PARSER_ERROR_WRONG_FORMAT, "Expected a JSON-object, got a: \"%s\"", json_node_type_name (root));
    g_object_unref (parser);
    g_object_unref (service);
    return;
  }

  json_node_seal (root);
  obj = json_node_get_object (root);

  self = rv_video_invidious_new ();

  if (g_task_return_error_if_cancelled (task)) {
    g_object_unref (self);
    g_object_unref (parser);
    g_object_unref (service);
    return;
  }

  RV_VIDEO_INVIDIOUS_PARSE_STRING ("videoId", video_id, "video-id");
  RV_VIDEO_INVIDIOUS_PARSE_STRING ("title", title, "title");
  // TODO: move to own metadata object like rv-item
  RV_VIDEO_INVIDIOUS_PARSE_STRING ("author", author, "author");

  // these are optional
  RV_VIDEO_INVIDIOUS_PARSE_OPTIONAL_STRING ("description", description, "description");

  // TODO: move hard-coded strings to constants
  // TODO: provide available video formats and move into an specialized list object
  // TODO: INVIDIOUS-Preference: Proxy Video
  // HACK: This is the macro inlined to provide this option.
  if (json_object_has_member (obj, "dashUrl")) { \
    self->video_uri = g_strdup_printf ("%s?local=true", json_object_get_string_member (obj, "dashUrl"));
    g_object_notify (self, "video-uri"); \
  } else {
    g_task_return_new_error (task, RV_SERVICE_JSON_PARSER_ERROR, RV_SERVICE_JSON_PARSER_ERROR_WRONG_FORMAT, "JSON-Object lacks the string member \"%s\", this JSON is invalid", "dashUrl"); \
    g_object_unref (self);
    g_object_unref (parser);
    g_object_unref (service);
    return;
  }

  self->published = json_object_get_int_member_with_default (obj, "published", 0);
  g_object_notify (self, "published");

  self->view_count = json_object_get_int_member_with_default (obj, "viewCount", 0);
  g_object_notify (self, "view-count");

  self->length = json_object_get_int_member_with_default (obj, "lengthSeconds", 0);
  g_object_notify (self, "length");

  self->recommended = rv_feed_invidious_new ();
  g_object_notify (self, "recommended");
  recommened_arr = json_object_get_array_member(obj, "recommendedVideos");
  if (g_task_return_error_if_cancelled (task)) {
    g_object_unref (self);
    g_object_unref (parser);
    g_object_unref (service);
    return;
  }

  rv_feed_invidious_insert_vids_from_json_array_async (self->recommended,
                                                       recommened_arr,
                                                       g_task_get_priority (task),
                                                       cancellable,
                                                       NULL,
                                                       NULL);

  if (g_task_return_error_if_cancelled (task)) {
    g_object_unref (self);
  } else {
    g_task_return_pointer (task, self, NULL);
  }
  g_object_unref (parser);
  g_object_unref (service);
}


void
rv_video_invidious_new_async (RvServiceInvidious  *service,
                              gchar               *videoid,
                              gint                 priority,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             userdata)
{
  GTask *task;
  gchar *api_uri;
  g_assert (RV_IS_INVIDIOUS (service));
  g_object_ref (service);

  task = g_task_new (service, cancellable, callback, userdata);
  g_task_set_priority (task, priority);

  api_uri = g_strdup_printf (API_VIDEOS, videoid);
  g_task_set_task_data (task, api_uri, g_free);

  g_task_run_in_thread (task, video_parser);
}

RvVideo *
rv_video_invidious_new_finish (RvServiceInvidious  *service,
                               GAsyncResult        *result,
                               GError             **error)
{
  g_return_val_if_fail (g_task_is_valid (result, service), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}
