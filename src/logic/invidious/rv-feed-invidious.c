/* rv-feed-invidious.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Rv.Service.Invidious.Feed"

#include "rv-feed-invidious.h"

#include <logic/rv-feed.h>
#include <logic/rv-item.h>

#include "rv-item-invidious.h""

static void rv_feed_invidious_iface_init (RvFeedInterface *iface);
static void rv_feed_invidious_list_model_init (GListModelInterface *iface);


struct _RvFeedInvidious
{
  GObject parent_instance;

  GListStore *store;

  gboolean continuable;
};


G_DEFINE_FINAL_TYPE_WITH_CODE (RvFeedInvidious, rv_feed_invidious, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (RV_TYPE_FEED, rv_feed_invidious_iface_init)
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, rv_feed_invidious_list_model_init));

typedef enum
{
  PROP_FEEDS_CONTINUABLE = 1,
  N_PROPS_FEEDS
};


static void
rv_feed_invidious_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  RvFeedInvidious *self = RV_FEED_INVIDIOUS (object);

  switch (prop_id)
    {
    case PROP_FEEDS_CONTINUABLE:
      g_value_set_boolean (value, self->continuable);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}


static void
insert_vids_from_json_array_impl (GTask *task,
                                  gpointer src,
                                  gpointer task_data,
                                  GCancellable *cancellable)
{
  g_assert (RV_IS_FEED_INVIDIOUS (src));
  RvFeedInvidious *self = (RvFeedInvidious*) src;
  JsonArray *arr = (JsonArray*) task_data; // wow, no way to check like the other :(

  guint len = json_array_get_length (arr);
  JsonObject *obj;
  RvItemInvidious *item;
  for (int i = 0; i < len; i++) {
    if (g_task_return_error_if_cancelled (task)) {
      return;
    }

    obj = json_array_get_object_element(arr, i);
    item = rv_item_invidious_new_from_json_object (obj);
    if (!RV_IS_ITEM_INVIDIOUS (item)) {
      continue;
    }
    g_assert (G_IS_LIST_STORE (self->store));

    g_list_store_append (G_LIST_STORE (self->store), RV_ITEM_INVIDIOUS (item));
  }
}


void
rv_feed_invidious_insert_vids_from_json_array_async (RvFeedInvidious     *self,
                                                     JsonArray           *arr,
                                                     gint                 priority,
                                                     GCancellable        *cancellable,
                                                     GAsyncReadyCallback *cb,
                                                     gpointer             user_data)
{
  g_assert (RV_IS_FEED_INVIDIOUS (self));

  json_array_ref (arr);
  // FIXME: SAFE API
  GTask *task =  g_task_new (self, cancellable, cb, user_data);
  g_task_set_task_data(task, arr, json_array_unref);
  g_task_set_priority(task, priority);

  g_task_run_in_thread (task, insert_vids_from_json_array_impl);
}


static void
rv_feed_invidious_dispose (GObject *gobject)
{
  RvFeedInvidious *self = RV_FEED_INVIDIOUS (gobject);

  g_clear_object (&self->store);

  G_OBJECT_CLASS (rv_feed_invidious_parent_class)->dispose (gobject);
}


static void
rv_feed_invidious_finalize (GObject *gobject)
{
  RvFeedInvidious *self = RV_FEED_INVIDIOUS (gobject);


  G_OBJECT_CLASS (rv_feed_invidious_parent_class)->finalize (gobject);
}


static void
rv_feed_invidious_class_init (RvFeedInvidiousClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = rv_feed_invidious_dispose;
  object_class->finalize = rv_feed_invidious_finalize;
  object_class->get_property = rv_feed_invidious_get_property;

  g_object_class_override_property(object_class, PROP_FEEDS_CONTINUABLE, "continuable");
}


static void
rv_feed_invidious_init (RvFeedInvidious *self)
{
  self->continuable = FALSE;


  self->store = g_list_store_new (RV_TYPE_ITEM_INVIDIOUS);
  g_signal_connect_swapped (self->store, "items-changed", g_list_model_items_changed, self);
}


static void
rv_feed_invidious_fetch_next_results_async (RvFeedInvidious *self,
                                            GCancellable *cancellable,
                                            GCallback callback,
                                            gpointer user_data)
{
  g_return_if_fail (RV_IS_FEED_INVIDIOUS (self));
  /*FIXME:SAFE API*/

}


static void
rv_feed_invidious_iface_init (RvFeedInterface *iface)
{
  iface->fetch_next_results_async = rv_feed_invidious_fetch_next_results_async;
}


guint
rv_feed_invidious_get_n_items (GListModel *list)
{
  RvFeedInvidious *self;

  g_assert (RV_IS_FEED_INVIDIOUS (list));
  self = RV_FEED_INVIDIOUS (list);
  /*FIXME:SAFE API*/

  return g_list_model_get_n_items (G_LIST_MODEL (self->store));
}


GType
rv_feed_invidious_get_item_type (GListModel *list)
{
  return RV_TYPE_ITEM;
}


gpointer
rv_feed_invidious_get_item (GListModel *list,
                            guint       position)
{
  RvFeedInvidious *self;

  g_assert (RV_IS_FEED_INVIDIOUS (list));
  self = RV_FEED_INVIDIOUS (list);
  /*FIXME:SAFE API*/

  return g_list_model_get_item (G_LIST_MODEL (self->store), position);
}

static void
rv_feed_invidious_list_model_init (GListModelInterface *iface)
{
  iface->get_item = rv_feed_invidious_get_item;
  iface->get_item_type = rv_feed_invidious_get_item_type;
  iface->get_n_items = rv_feed_invidious_get_n_items;
}

RvFeedInvidious *
rv_feed_invidious_new ()
{
  return g_object_new (RV_TYPE_FEED_INVIDIOUS, NULL);
}
