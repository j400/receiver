/* rv-invidious.h
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS

#define RV_TYPE_INVIDIOUS (rv_invidious_get_type())
G_DECLARE_FINAL_TYPE (RvServiceInvidious, rv_invidious, RV, INVIDIOUS, GObject)

typedef gchar* (*InvidiousInstanceURIGetterCallback) (GObject*);
void rv_invidious_get_default_async (GObject *src, gboolean force_new, GCancellable *cancellable, InvidiousInstanceURIGetterCallback cb_get_instance_url, GCallback cb, gpointer user_data);

gpointer rv_invidious_get_default_finish (RvServiceInvidious *self, GAsyncResult *result, GError **error);

gboolean rv_invidious_is_ready (RvServiceInvidious*);
gchar * rv_invidious_get_instance_url (RvServiceInvidious*);
JsonParser* rv_invidious_api (RvServiceInvidious *self, const gchar *api_endpoint, GCancellable *cancellable, GError **error);
G_END_DECLS

