/* rv-feed-invidious.h
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define RV_TYPE_FEED_INVIDIOUS (rv_feed_invidious_get_type())
G_DECLARE_FINAL_TYPE (RvFeedInvidious, rv_feed_invidious, RV, FEED_INVIDIOUS, GObject)

RvFeedInvidious *rv_feed_invidious_new (void);
void rv_feed_invidious_insert_vids_from_json_array_async (RvFeedInvidious*, JsonArray*, gint, GCancellable*, GAsyncReadyCallback*, gpointer);



G_END_DECLS
