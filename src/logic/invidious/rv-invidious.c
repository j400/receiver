/* rv-invidious.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Rv.Service.Invidious"

#include "rv-invidious.h"

#include <gio/gio.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include <logic/rv-feed.h>
#include <logic/rv-errors.h>
#include <logic/rv-item.h>
#include <logic/rv-service.h>
#include <logic/rv-video.h>

#include "invidious-api.h"
#include "rv-feed-invidious.h"
#include "rv-item-invidious.h"
#include "rv-video-invidious.h"


/* GObject RvServiceInvidious */

struct _RvServiceInvidious
{
  GObject  parent_instance;

  gchar       *url;
  gboolean     ready;
  SoupSession *session;
};

static void rv_service_invidious_init_iface (RvServiceInterface *iface);
G_DEFINE_FINAL_TYPE_WITH_CODE (RvServiceInvidious, rv_invidious, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE(RV_TYPE_SERVICE, rv_service_invidious_init_iface))


enum
{
  PROP_0,
  PROP_URL = 1,
  PROP_READY,
  N_PROPS
};

static GParamSpec *props[N_PROPS] = { NULL, };

static void
rv_invidious_set_property (GObject      *object,
                           guint         property_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  RvServiceInvidious *self = RV_INVIDIOUS (object);

  switch (property_id)
    {
      case PROP_URL:
        g_free (self->url);
        self->url = g_value_dup_string (value);
        g_info ("url set to %s\n", self->url);
        break;

      default :
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
rv_invidious_get_property (GObject    *object,
                           guint       property_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  RvServiceInvidious *self = RV_INVIDIOUS (object);

  switch (property_id)
    {
      case PROP_URL:
        g_value_set_string(value, rv_invidious_get_instance_url(self));
        break;

      case PROP_READY:
        g_value_set_boolean (value, rv_invidious_is_ready(self));
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
rv_invidious_dispose (GObject *gobject)
{
  RvServiceInvidious *self = RV_INVIDIOUS (gobject);

  g_clear_object (&self->session);

  G_OBJECT_CLASS (rv_invidious_parent_class)->dispose (gobject);
}

static void
rv_invidious_finalize (GObject *gobject)
{
  RvServiceInvidious *self = RV_INVIDIOUS (gobject);

  g_free (self->url);

  G_OBJECT_CLASS (rv_invidious_parent_class)->finalize (gobject);
}

static void
rv_invidious_class_init (RvServiceInvidiousClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);


  object_class->set_property = rv_invidious_set_property;
  object_class->get_property = rv_invidious_get_property;
  object_class->dispose      = rv_invidious_dispose;
  object_class->finalize     = rv_invidious_finalize;

  props[PROP_URL] =
    g_param_spec_string ("url", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE|
                          G_PARAM_CONSTRUCT_ONLY|
                          G_PARAM_STATIC_STRINGS));

  props[PROP_READY] =
    g_param_spec_boolean ("ready", NULL, NULL,
                          FALSE,
                          (G_PARAM_READABLE|
                           G_PARAM_STATIC_STRINGS|
                           G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, props);
}


static void
rv_invidious_init (RvServiceInvidious *self)
{
  self->ready = FALSE;

  self->session = soup_session_new ();
  // faster timeout
  soup_session_set_timeout (self->session, 10);
  soup_session_set_idle_timeout (self->session, 10);
  // logging // FIXME: destroy logger
  SoupLogger *logger = soup_logger_new (SOUP_LOGGER_LOG_HEADERS);
  soup_session_add_feature (self->session, logger);
}

JsonParser*
rv_invidious_api (RvServiceInvidious *self,
                  const gchar *api_endpoint,
                  GCancellable *cancellable,
                  GError **error)
{

  gchar *uri;
  JsonParser *parser;
  SoupMessage *msg;
  GInputStream *stream;

  uri = g_strdup_printf ("%s%s", self->url, api_endpoint);
  g_debug ("Using this endpoint: %s\n", uri);

  msg = soup_message_new (SOUP_METHOD_GET, uri);
  g_free (uri);

  if (g_cancellable_set_error_if_cancelled (cancellable, error)) {
    g_object_unref (msg);
    return NULL;
  }

  stream = soup_session_send (self->session, msg, cancellable, error);
  g_object_unref (msg);
  if (*error != NULL) {
    g_error ("Failed to download: %s", (*error)->message);
    return NULL;
  }

  parser = json_parser_new ();
  json_parser_load_from_stream (parser, stream, cancellable, error);
  g_object_unref (stream);
  if (*error != NULL) {
    g_error ("Failed to parse JSON: %s", (*error)->message);
    g_object_unref (parser);
    return NULL;
  }

  if (g_cancellable_set_error_if_cancelled (cancellable, error)) {
    g_object_unref (parser);
    return NULL;
  }

  return parser;
}

void
rv_invidious_check_availability (GTask *task,
                                 RvServiceInvidious *self,
                                 gpointer task_data,
                                 GCancellable *cancellable)
{
  GError *error = NULL;

  JsonParser* parser = rv_invidious_api (self, API_STATS, cancellable, &error);
  if (error != NULL) {
    g_task_return_error (task, error);
    g_object_unref (parser);
    return;
  }

  JsonNode *root = json_parser_get_root(parser);

  JsonNodeType roottype = json_node_get_node_type (root);

  g_return_if_fail (roottype == JSON_NODE_OBJECT);

  JsonObject *stats = json_node_get_object (root);

  JsonObject *software = json_object_get_object_member(stats, "software");

  g_info ("Invidious instance version: %s\n", json_object_get_string_member (software, "version"));

  g_object_unref (parser);
  g_task_return_boolean (task, TRUE);

  self->ready = TRUE;
  g_object_notify_by_pspec ((GObject*)self, props[PROP_READY]);
}

void
rv_invidious_check_availability_async (RvServiceInvidious *self,
                                       gint                  priority,
                                       GCancellable         *cancellable,
                                       GAsyncReadyCallback   callback,
                                       gpointer              user_data)
{
  GTask *task;

  g_return_if_fail (RV_IS_INVIDIOUS (self));

  task = g_task_new (self, cancellable, callback, user_data);

  g_task_set_priority (task, priority);

  g_task_run_in_thread (task, rv_invidious_check_availability);
}


RvServiceInvidious*
rv_invidious_new (gchar *url)
{
  return g_object_new (RV_TYPE_INVIDIOUS,
                       "url", url,
                       NULL);
}

gboolean
rv_invidious_check_availability_finish (RvServiceInvidious  *self,
                                        GAsyncResult        *result,
                                        GError             **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), NULL);

  return g_task_propagate_boolean (G_TASK (result), error);
}

gpointer
rv_invidious_get_default_finish (RvServiceInvidious  *self,
                                      GAsyncResult        *result,
                                      GError             **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}


void
default_cb (GObject      *source_object,
            GAsyncResult *res,
            gpointer      user_data)
{
  GError *error = NULL;
  g_assert (RV_IS_INVIDIOUS (source_object));

  GTask *task = G_TASK (user_data);

  rv_invidious_check_availability_finish (source_object, res, &error);
  if (error != NULL) {
    g_task_return_error (task, error);
    return;
  }
  g_task_return_pointer (task, source_object, NULL);
}

void
rv_invidious_get_default_async (GObject *src, gboolean force_new,
                                GCancellable *cancellable,
                                InvidiousInstanceURIGetterCallback cb_get_instance_url,
                                GCallback cb, gpointer user_data)
{
  static RvServiceInvidious *instance = NULL;
  GError *error = NULL;

  if (force_new && instance != NULL) {
      g_clear_object (&instance);
  }

  if (instance == NULL) {
    gchar *url = cb_get_instance_url(src);
    instance = rv_invidious_new (url);
    g_object_add_weak_pointer (G_OBJECT (instance), (gpointer *) &instance);

    GUri *uri = g_uri_parse (url, G_URI_FLAGS_NONE, NULL);
    g_free (url);
    if (uri == NULL) {
      GTask *task = g_task_new (instance, cancellable, cb, user_data);
      error = g_error_new (RV_SERVICE_INIT_ERROR, RV_SERVICE_INIT_ERROR_INVALID_URI, "Invalid uri");
      g_task_return_error (task, error);
      return;
    }

    GTask *task = g_task_new (instance, cancellable, cb, user_data);
    rv_invidious_check_availability_async (instance, 0, cancellable, default_cb, task);
    return;
  } else {
    /* an instance does already exist and it works! :) */
    GTask *task = g_task_new (instance, cancellable, cb, user_data);
    g_task_return_pointer (task, instance, NULL);
    return;
  }

  g_assert_not_reached ();
}

gboolean
rv_invidious_is_ready (RvServiceInvidious *self)
{
  g_assert (RV_IS_INVIDIOUS (self));
  return self->ready;
}

gchar*
rv_invidious_get_instance_url (RvServiceInvidious *self)
{
  g_assert (RV_IS_INVIDIOUS (self));
  return g_strdup (self->url);
}


static void
rv_service_invidious_init_iface (RvServiceInterface *iface)
{
  iface->new_video_async = rv_video_invidious_new_async;
  iface->new_video_finish = rv_video_invidious_new_finish;
}
