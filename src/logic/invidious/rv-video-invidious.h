/* rv-video-invidious.h
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "rv-invidious.h"
#include "../rv-video.h"

G_BEGIN_DECLS

#define RV_TYPE_VIDEO_INVIDIOUS (rv_video_invidious_get_type())

G_DECLARE_FINAL_TYPE (RvVideoInvidious, rv_video_invidious, RV, VIDEO_INVIDIOUS, GObject)

void rv_video_invidious_new_async (RvServiceInvidious*, gchar*, gint, GCancellable*, GAsyncReadyCallback, gpointer);
RvVideo* rv_video_invidious_new_finish (RvServiceInvidious*, GAsyncResult*, GError **);

G_END_DECLS
