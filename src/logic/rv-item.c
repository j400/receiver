/* rv-item.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Rv.Model.Item"

#include "rv-item.h"
#include "rv-thumbnail-list.h"

G_DEFINE_INTERFACE (RvItem, rv_item, G_TYPE_OBJECT)

static void
rv_item_default_init (RvItemInterface *iface)
{
  g_object_interface_install_property (iface,
                                       g_param_spec_string
                                       ("uri", NULL, NULL,
                                        NULL,
                                        (G_PARAM_READWRITE|
                                         G_PARAM_CONSTRUCT_ONLY|
                                         G_PARAM_STATIC_STRINGS|
                                         G_PARAM_EXPLICIT_NOTIFY)
                                       )
                                      );

  g_object_interface_install_property (iface,
                                       g_param_spec_string
                                       ("title", NULL, NULL,
                                        NULL,
                                        (G_PARAM_READWRITE|
                                         G_PARAM_CONSTRUCT_ONLY|
                                         G_PARAM_STATIC_STRINGS|
                                         G_PARAM_EXPLICIT_NOTIFY)
                                       )
                                      );

  // FIXME: Too video specific, split into VideoItem
  g_object_interface_install_property (iface,
                                       g_param_spec_string
                                        ("creator", NULL, NULL,
                                         NULL,
                                         (G_PARAM_READWRITE|
                                          G_PARAM_CONSTRUCT_ONLY|
                                          G_PARAM_STATIC_STRINGS|
                                          G_PARAM_EXPLICIT_NOTIFY)
                                         )
                                       );

  g_object_interface_install_property (iface,
                                       g_param_spec_object
                                        ("thumbnails", NULL, NULL,
                                         RV_TYPE_THUMBNAIL_LIST,
                                         (G_PARAM_READWRITE|
                                          G_PARAM_CONSTRUCT_ONLY|
                                          G_PARAM_STATIC_STRINGS|
                                          G_PARAM_EXPLICIT_NOTIFY)
                                        ));
}
