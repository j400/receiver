/* rv-recommended.c
 *
 * Copyright 2022 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-recommended.h"

#include "logic/rv-video.h"

#include <adwaita.h>

#include "rv-item-view.h"

struct _RvRecommended
{
  AdwBin parent_instance;

  GtkStack *stack;
  AdwStatusPage *page_loading;
  AdwStatusPage *page_no_results;
  GtkScrolledWindow *scrolled_window;

  GtkListView *list_view;
  GListModel *src;

  GtkSizeGroup *image_size_group;
};

G_DEFINE_FINAL_TYPE (RvRecommended, rv_recommended, ADW_TYPE_BIN)

enum {
  PROP_SRC = 1,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
rv_recommended_dispose (GObject *object)
{
  RvRecommended *self = (RvRecommended *)object;

  g_clear_object (&self->src);
  //g_clear_object (&self->list_view);
  g_clear_object (&self->image_size_group);

  G_OBJECT_CLASS (rv_recommended_parent_class)->dispose (object);
}

static void
rv_recommended_finalize (GObject *object)
{
  RvRecommended *self = (RvRecommended *)object;

  G_OBJECT_CLASS (rv_recommended_parent_class)->finalize (object);
}

static void
setup_listitem_cb (GtkListItemFactory *factory,
                   GtkListItem        *list_item,
                   RvRecommended      *self)
{
  RvItemView *view = rv_item_view_new (self->image_size_group);
  gtk_list_item_set_child (list_item, GTK_WIDGET (view));
  rv_item_view_set_model (view, NULL);
}

static void
bind_listitem_cb (GtkListItemFactory *factory,
                  GtkListItem        *list_item,
                  gpointer)
{
  RvItemView *view;
  RvItem *item;

  view = RV_ITEM_VIEW (gtk_list_item_get_child (list_item));
  item = RV_ITEM (gtk_list_item_get_item (list_item));
  rv_item_view_set_model (view, item);
}

static void
activate_listitem_cb (GtkListView   *list,
                      guint          position,
                      RvRecommended *self)
{
  gchar *uri;
  GListModel *model = self->src;

  GObject *item_gobj = g_list_model_get_object (model, position);
  g_assert (RV_IS_ITEM (item_gobj));
  RvItem *item = RV_ITEM (item_gobj);

  g_object_get (G_OBJECT (item), "uri", &uri, NULL);
  gtk_widget_activate_action (self, "tab.open", G_VARIANT_TYPE_STRING, uri, NULL);

  g_free (uri);
}

static void
rv_recommended_set_src (RvRecommended *self,
                        GListModel    *src)
{
  if (self->src != NULL) {
    g_clear_object (&self->src);
  }

  self->src = src;

  if (self->src != NULL) {
    g_object_ref(self->src);

    /* Set child: loading */
    gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->page_loading));

    /* FIXME: there's a bug involving memory allocation and gtk's list box'
     * in console: Trying to snapshot GtkListItemWidget 0x55b668336050 without a current allocation
     */
    gtk_list_view_set_model (self->list_view, GTK_SELECTION_MODEL (gtk_single_selection_new (self->src)));

    gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->scrolled_window));
  } else {
    /* self->src == NULL */
    /* Set child: No results */
    gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->page_no_results));
  }
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SRC]);
}

static void
rv_recommended_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  RvRecommended *self = RV_RECOMMENDED (object);

  switch (prop_id)
    {
    case PROP_SRC:
      g_value_set_object (value, self->src);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_recommended_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  RvRecommended *self = RV_RECOMMENDED (object);

  switch (prop_id)
    {
    case PROP_SRC:
      rv_recommended_set_src(self, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_recommended_class_init (RvRecommendedClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = rv_recommended_dispose;
  object_class->finalize = rv_recommended_finalize;
  object_class->get_property = rv_recommended_get_property;
  object_class->set_property = rv_recommended_set_property;

  properties[PROP_SRC] = g_param_spec_object ("src",
                                              NULL, NULL,
                                              G_TYPE_LIST_MODEL,
                                              G_PARAM_READWRITE |
                                              G_PARAM_EXPLICIT_NOTIFY |
                                              G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/app/drey/Receiver/ui/rv-recommended.ui");
  gtk_widget_class_bind_template_child (widget_class, RvRecommended, stack);
  gtk_widget_class_bind_template_child (widget_class, RvRecommended, page_loading);
  gtk_widget_class_bind_template_child (widget_class, RvRecommended, page_no_results);
  gtk_widget_class_bind_template_child (widget_class, RvRecommended, scrolled_window);
}

static void
rv_recommended_init (RvRecommended *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->image_size_group = gtk_size_group_new (GTK_SIZE_GROUP_VERTICAL);

  GtkListItemFactory* factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup_listitem_cb), self);
  g_signal_connect (factory, "bind", G_CALLBACK (bind_listitem_cb), NULL);

  self->list_view = GTK_LIST_VIEW (gtk_list_view_new (NULL, factory));
  gtk_list_view_set_single_click_activate (self->list_view, TRUE);
  g_signal_connect (self->list_view, "activate", G_CALLBACK (activate_listitem_cb), self);
  GStrvBuilder *strvbuilder = g_strv_builder_new ();
  char **strv_old = gtk_widget_get_css_classes (self->list_view);
  g_strv_builder_addv(strvbuilder, strv_old);
  g_strv_builder_add(strvbuilder, "recommended");
  GStrv *strv = g_strv_builder_end (strvbuilder);
  gtk_widget_set_css_classes(self->list_view, strv);


  gtk_scrolled_window_set_child (self->scrolled_window, GTK_WIDGET (self->list_view));
}
