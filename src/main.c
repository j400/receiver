/* main.c
 *
 * Copyright 2022-2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-config.h"

#include <glib/gi18n.h>
#include <gst/gst.h>

#include "rv-application.h"

void
rv_init ();

int
main (int   argc,
      char *argv[])
{
  g_autoptr(RvApplication) app = NULL;
  int ret;

  /* Set up gettext translations */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  gst_init (&argc, &argv);

  rv_init ();

  app = rv_application_new ("app.drey.Receiver", G_APPLICATION_HANDLES_OPEN);

  ret = g_application_run (G_APPLICATION (app), argc, argv);

  return ret;
}

