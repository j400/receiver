/*
 * rv-thumbnail.h
 * Note: keeping the same license as rv-thumbnail.c as it's forked from GTK
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */
#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define RV_TYPE_PICTURE (rv_picture_get_type())

G_DECLARE_FINAL_TYPE (RvPicture, rv_picture, RV, PICTURE, GtkWidget)

const char *
rv_picture_get_alternative_text (RvPicture *self);
void
rv_picture_set_alternative_text (RvPicture *self,
                                  const char *alternative_text);
GdkPaintable *
rv_picture_get_paintable (RvPicture *self);
void
rv_picture_set_paintable (RvPicture   *self,
                           GdkPaintable *paintable);
GtkWidget*
rv_picture_new_for_paintable (GdkPaintable *paintable);
GtkWidget*
rv_picture_new (void);

G_END_DECLS
