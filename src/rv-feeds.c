/* rv-feeds.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Receiver.Feeds"

#include "rv-feeds.h"

struct _RvFeeds
{
  AdwBin parent_instance;

  AdwViewStack *mainstack;

  /* template widgets */
  AdwViewSwitcherTitle *switcher_title;
};

typedef enum
{
  N_PROPERTIES = 1
} RvFeedsProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

G_DEFINE_TYPE (RvFeeds, rv_feeds, ADW_TYPE_BIN)

static void
rv_feeds_set_property (GObject      *object,
                       guint         property_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  RvFeeds *self = RV_FEEDS (object);

  switch ((RvFeedsProperty) property_id)
    {
      default :
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
rv_feeds_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  RvFeeds *self = RV_FEEDS (object);

  switch ((RvFeedsProperty) property_id)
    {
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}


static void
rv_feeds_finalize (GObject *gobject)
{
  RvFeeds *self = RV_FEEDS (gobject);

  g_clear_object (&self->mainstack);

  G_OBJECT_CLASS (rv_feeds_parent_class)->finalize (gobject);
}

static void
rv_feeds_class_init (RvFeedsClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gobject_class->finalize = rv_feeds_finalize;
  gobject_class->set_property = rv_feeds_set_property;
  gobject_class->get_property = rv_feeds_get_property;

  //g_object_class_install_properties (gobject_class, N_PROPERTIES, obj_properties);
  gtk_widget_class_set_template_from_resource (widget_class, "/app/drey/Receiver/ui/rv-feeds.ui");
}

static void
rv_feeds_init (RvFeeds *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
