/* rv-item-view.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-item-view.h"

#include <libsoup/soup.h>

#include "logic/rv-item.h"
#include "logic/rv-thumbnail-list.h"

#include "rv-picture.h"

struct _RvItemView
{
  AdwBin parent_instance;

  GtkLabel *title;
  GtkLabel *author;
  GtkOverlay *pictureoverlay;
  GtkSpinner *spinner;
  RvPicture *picture;

  RvItem *model;
  SoupSession *session;
};

G_DEFINE_FINAL_TYPE (RvItemView, rv_item_view, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_MODEL,
  PROP_SIZE_GROUP,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

RvItemView *
rv_item_view_new (GtkSizeGroup *size_group)
{
  return g_object_new (RV_TYPE_ITEM_VIEW,
                       "size-group", size_group,
                       NULL);
}

static void
rv_item_view_dispose (GObject *object)
{
  RvItemView *self = (RvItemView *)object;

  g_clear_object (&self->model);
  g_clear_object (&self->session);

  G_OBJECT_CLASS (rv_item_view_parent_class)->dispose (object);
}

static void
rv_item_view_finalize (GObject *object)
{
  RvItemView *self = (RvItemView *)object;

  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (rv_item_view_parent_class)->finalize (object);
}

static void
rv_item_view_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  RvItemView *self = RV_ITEM_VIEW (object);

  switch (prop_id)
    {
    case PROP_MODEL:
      g_value_set_object (value, self->model);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

void
fetch_thumbnail (GTask        *task,
                 RvItemView   *self,
                 gpointer      url,
                 GCancellable *cancellable)
{
  SoupMessage *msg;
  GError *error = NULL;
  GBytes *bytes;
  GdkTexture *texture;

  g_debug ("Fetching thumbnail from: %s\n", url);

  msg = soup_message_new (SOUP_METHOD_GET, url);

  if (g_cancellable_set_error_if_cancelled (cancellable, error)) {
    g_warning ("Failed fetching thumbnail: 1");
    g_object_unref (msg);
    g_task_return_error (task, error);
    return;
  }

  bytes = soup_session_send_and_read (self->session, msg, cancellable, &error);
  g_object_unref (msg);
  if (error != NULL) {
    g_warning ("Failed fetching thumbnail: 2");
    g_task_return_error (task, error);
    return;
  }

  texture = gdk_texture_new_from_bytes (bytes, &error);
  if (error != NULL) {
    g_task_return_error (task, error);
    return;
  }

  g_task_return_pointer (task, texture, NULL);
  return;
}

void
fetch_thumbnail_done (GObject      *source_object,
                      GAsyncResult *res,
                      gpointer)
{
  g_assert (RV_IS_ITEM_VIEW (source_object));
  g_object_unref (source_object);
  RvItemView *self = source_object;
  GError *error = NULL;

  GdkTexture *texture = g_task_propagate_pointer (res, &error);
  if (error) {
    return;
  }

  rv_picture_set_paintable (self->picture, texture);
  gtk_widget_set_visible (self->spinner, FALSE);
}

// TODO: remove from frontend, see /src/logic/meson.build
void
rv_item_view_fetch_thumbnail_async (RvItemView *self, const gchar *url)
{
  g_assert (RV_IS_ITEM_VIEW (self));
  g_object_ref (self);
  GTask *task;

  g_debug ("Fetching thumbnail from: %s\n", url);
  task = g_task_new (self, NULL, fetch_thumbnail_done, NULL);
  g_task_set_task_data (task, g_strdup (url), g_free);
  g_task_run_in_thread (task, fetch_thumbnail);
}

void
rv_item_view_set_model (RvItemView *self, RvItem *model)
{
  g_assert (RV_IS_ITEM_VIEW (self));
  self->model = model;

  if (self->model == NULL) {
    gtk_label_set_label (self->title, "");
    gtk_label_set_label (self->author, "");
    gtk_widget_set_visible (self->spinner, TRUE);
    rv_picture_set_paintable (self->picture, gdk_texture_new_from_resource ("/app/drey/Receiver/black.jpg"));
    g_object_notify_by_pspec (self, properties[PROP_MODEL]);
    return;
  } else {
    g_object_bind_property (self->model, "title",
                            self->title, "label",
                            G_BINDING_SYNC_CREATE);

    g_object_bind_property (self->model, "creator",
                            self->author, "label",
                            G_BINDING_SYNC_CREATE);

    g_object_notify_by_pspec (self, properties[PROP_MODEL]);

    // TODO: select sensible sized thumbnail
    RvThumbnailList *thumbs;
    g_object_get (self->model, "thumbnails", &thumbs, NULL);
    if (g_list_model_get_n_items (thumbs) > 0) {
      RvThumbnail *thumb = g_list_model_get_object (thumbs, 0);
      const gchar *url = rv_thumbnail_get_url (thumb);
      rv_item_view_fetch_thumbnail_async (self, url);
    }
    return;
  }
}

void
rv_item_view_set_size_group (RvItemView *self, GtkSizeGroup *size_group)
{
  g_return_if_fail (size_group != NULL);

  //gtk_size_group_add_widget (size_group, self->picture);

  g_object_notify_by_pspec (self, properties[PROP_SIZE_GROUP]);
}

static void
rv_item_view_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  RvItemView *self = RV_ITEM_VIEW (object);

  switch (prop_id)
    {
    case PROP_MODEL:
      rv_item_view_set_model (self, g_value_get_object (value));
      break;
    case PROP_SIZE_GROUP:
      rv_item_view_set_size_group (self, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_item_view_class_init (RvItemViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rv_item_view_finalize;
  object_class->get_property = rv_item_view_get_property;
  object_class->set_property = rv_item_view_set_property;

  properties [PROP_MODEL] =
    g_param_spec_object ("model",
                         NULL, NULL,
                         RV_TYPE_ITEM,
                         (G_PARAM_READWRITE |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_SIZE_GROUP] =
    g_param_spec_object ("size-group",
                         NULL, NULL,
                         GTK_TYPE_SIZE_GROUP,
                         (G_PARAM_WRITABLE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));
  // readwrite for debugging, writeonly would be enough

  g_object_class_install_properties (object_class, N_PROPS, properties);

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/app/drey/Receiver/ui/rv-item-view.ui");
  gtk_widget_class_bind_template_child (widget_class, RvItemView, title);
  gtk_widget_class_bind_template_child (widget_class, RvItemView, author);
  gtk_widget_class_bind_template_child (widget_class, RvItemView, picture);
  gtk_widget_class_bind_template_child (widget_class, RvItemView, pictureoverlay);
  gtk_widget_class_bind_template_child (widget_class, RvItemView, spinner);
}

static void
rv_item_view_init (RvItemView *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->session = soup_session_new ();
}
