/* rv-preferences.c
 *
 * Copyright 2023 Jane Rachinger <jane400@bingo-ev.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rv-preferences.h"
#include <adwaita.h>

struct _RvPreferences
{
  AdwPreferencesWindow parent_instance;

  AdwEntryRow *invidious_url;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (RvPreferences, rv_preferences, ADW_TYPE_PREFERENCES_WINDOW)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

enum {
  INVIDIOUS_URL_APPLY,
  N_SIGNALS,
};

static gint signals [N_SIGNALS];

static void
rv_preferences_finalize (GObject *object)
{
  RvPreferences *self = (RvPreferences *)object;

  G_OBJECT_CLASS (rv_preferences_parent_class)->finalize (object);
}

static void
rv_preferences_dispose (GObject *object)
{
  RvPreferences *self = (RvPreferences *)object;

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (rv_preferences_parent_class)->dispose (object);
}

static void
rv_preferences_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  RvPreferences *self = RV_PREFERENCES (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
rv_preferences_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  RvPreferences *self = RV_PREFERENCES (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
on_invidious_url_apply (AdwEntryRow *entryrow, RvPreferences *self)
{
  const char *url = gtk_editable_get_text (GTK_EDITABLE (entryrow));

  g_settings_set_string (self->settings, "invidious-url", url);
  g_settings_apply (self->settings);

  g_signal_emit (self, signals[INVIDIOUS_URL_APPLY], 0);
}

static void
rv_preferences_class_init (RvPreferencesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rv_preferences_finalize;
  object_class->dispose = rv_preferences_dispose;
  object_class->get_property = rv_preferences_get_property;
  object_class->set_property = rv_preferences_set_property;

  signals [INVIDIOUS_URL_APPLY] =
    g_signal_new ("invidious-url-apply",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/app/drey/Receiver/ui/rv-preferences.ui");
  gtk_widget_class_bind_template_child (widget_class, RvPreferences, invidious_url);
}

static void
rv_preferences_init (RvPreferences *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("app.drey.Receiver");

  g_signal_connect (self->invidious_url, "apply", on_invidious_url_apply, self);
}
