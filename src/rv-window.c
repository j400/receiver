/* rv-window.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Receiver.Window"

#include "rv-window.h"

#include "logic/rv-video.h"

#include <glib.h>
#include <json-glib/json-glib.h>

#include "rv-application.h"
#include "rv-feeds.h"
#include "rv-tab.h"

struct _RvWindow
{
  AdwApplicationWindow  parent_instance;

  AdwViewStack *stack_all;
  AdwTabOverview *tab_overview;
  AdwTabView *tab_view;

  RvPreferences *preferences;
};

G_DEFINE_TYPE (RvWindow, rv_window, ADW_TYPE_APPLICATION_WINDOW)

enum
{
  PROP_0,
  PROP_VIEW,
  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void
rv_window_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  RvWindow *self = RV_WINDOW (object);

  switch (property_id)
    {
      default :
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
rv_window_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  RvWindow *self = RV_WINDOW (object);

  switch (property_id)
    {
      case PROP_VIEW:
        g_value_set_object (value, self->tab_view);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
rv_window_finalize (GObject *gobject)
{
  RvWindow *self = RV_WINDOW (gobject);

  G_OBJECT_CLASS (rv_window_parent_class)->finalize (gobject);
}

static void
rv_window_show_tab_overview (GtkWidget  *widget,
                             const char *action_name,
                             GVariant   *parameter)
{
  g_assert (RV_IS_WINDOW (widget));
  RvWindow *self = RV_WINDOW (widget);

  adw_view_stack_set_visible_child (self->stack_all, GTK_WIDGET (self->tab_overview));
  adw_tab_overview_set_open (self->tab_overview, TRUE);
}

AdwTabView *
cb_create_window (AdwTabView *self,
                  gpointer user_data)
{
  RvWindow *parent = RV_WINDOW (user_data);

  RvWindow *window = rv_window_new (gtk_window_get_application (parent));
  return window->tab_view;
}

// AdwTabPage*
// cb_create_tab (AdwTabOverview *self,
//                gpointer        user_data);

RvWindow *
rv_window_new (GtkApplication *app)
{
  g_assert (GTK_IS_APPLICATION (app));
  return g_object_new (RV_TYPE_WINDOW,
                       "application", app,
                       NULL);
}


static void
rv_window_class_init (RvWindowClass *klass)
{
  GObjectClass *class_object = G_OBJECT_CLASS (klass);
  GtkWidgetClass *class_widget = GTK_WIDGET_CLASS (klass);

  class_object->set_property = rv_window_set_property;
  class_object->get_property = rv_window_get_property;
  class_object->finalize = rv_window_finalize;

  obj_properties[PROP_VIEW] =
    g_param_spec_object ("view", NULL, NULL,
                         ADW_TYPE_TAB_VIEW,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (class_object, N_PROPERTIES, obj_properties);

  gtk_widget_class_set_template_from_resource (class_widget, "/app/drey/Receiver/ui/rv-window.ui");
  gtk_widget_class_bind_template_child (class_widget, RvWindow, stack_all);
  gtk_widget_class_bind_template_child (class_widget, RvWindow, tab_overview);
  gtk_widget_class_bind_template_child (class_widget, RvWindow, tab_view);
  gtk_widget_class_install_action (class_widget, "win.taboverview.open", NULL, rv_window_show_tab_overview);
}


static void
rv_window_init (RvWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect (self->tab_view, "create-window", cb_create_window, self);
  // g_signal_connect (self->tab_overview, "create-tab", cb_create_tab, self);
}

RvTab *
rv_window_new_tab (RvWindow *self)
{
  AdwTabPage *page;
  RvTab *tab;

  tab = rv_tab_new ();
  page = adw_tab_view_add_page (self->tab_view, tab, NULL);
  g_object_set (G_OBJECT (tab), "tab-page", page, NULL);
  return tab;
}

void
rv_window_new_tab_with_uri (RvWindow *self, GUri *uri)
{
  RvTab *tab = rv_window_new_tab (self);
  rv_tab_open_uri (tab, uri);
}

RvWindow *
rv_window_get_by_widget (GtkWidget *widget)
{
  GtkRoot *root = gtk_widget_get_root (widget);
  g_assert (RV_IS_WINDOW (root));
  return (RvWindow*) root;
}

static void
on_preferences_window_close_request (GtkWindow*, RvWindow *self)
{
  self->preferences = NULL;
}

RvPreferences*
rv_window_get_preferences_window (RvWindow *self)
{
  g_assert (RV_IS_WINDOW (self));

  if (self->preferences == NULL) {
    self->preferences = g_object_new (RV_TYPE_PREFERENCES,
                                      "parent", self,
                                      NULL);
    g_signal_connect (self->preferences, "close-request", on_preferences_window_close_request, self);
  }

  return self->preferences;
}
