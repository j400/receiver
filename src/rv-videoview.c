/* rv-video.c
 *
 * Copyright 2022 Jane Rachinger <jane400@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Receiver.VideoView"

#include "rv-videoview.h"

#include "logic/rv-video.h"

#include "rv-application.h"
#include "rv-recommended.h"
#include "rv-player.h"
#include "rv-tab-content.h"

struct _RvVideoView
{
  AdwBin parent_instance;

  RvVideo  *video;
  gchar    *vidid;
  gboolean  needs_switcher;
  gboolean  bottom_switcher;

  /* template widgets */
  GtkStack      *stack;
  GtkBox        *page_welcome;
  GtkBox        *page_loading;
  AdwFlap *page_video;

  AdwTabView *view;

  RvPlayer *player;
  RvRecommended *recommended;

  gchar *title;
};

static void
rv_video_view_init_iface (RvVideoInterface*)
{}

G_DEFINE_FINAL_TYPE_WITH_CODE (RvVideoView, rv_video_view, ADW_TYPE_BIN,
                               G_IMPLEMENT_INTERFACE(RV_TYPE_TAB_CONTENT, rv_video_view_init_iface))

typedef enum
{
  PROP_VIDEO = 1,
  PROP_STACK,
  PROP_WANTS_STACK_SWITCHER,
  PROP_USE_BOTTOM_SWITCHER,
  PROP_VIEW,
  PROP_TITLE,
  N_PROPERTIES
} RvVideoViewProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

void
rv_video_view_set_video (RvVideoView *self,
                         RvVideo *video)
{
  // Remove old and set new object
  g_clear_object (&self->video);
  self->video = video;
  g_object_ref (self->video);

  if (self->video == NULL) {
    // If the object is NULL, then we're probably loading it.
    gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->page_loading));
  } else {
    gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->page_video));
  }

  g_object_notify_by_pspec (G_OBJECT (self), obj_properties[PROP_VIDEO]);
}

static void
rv_video_view_set_property (GObject      *object,
                            guint         property_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  RvVideoView *self = RV_VIDEO_VIEW (object);

  switch ((RvVideoViewProperty) property_id)
    {
      case PROP_VIDEO:
        rv_video_view_set_video (self, g_value_get_object (value));
        break;
      case PROP_WANTS_STACK_SWITCHER:
        self->needs_switcher = g_value_get_boolean (value);
        break;
      case PROP_USE_BOTTOM_SWITCHER:
        self->bottom_switcher = g_value_get_boolean (value);
        break;
      case PROP_VIEW:
        self->view = g_value_get_object (value);
        break;
      case PROP_TITLE:
        g_clear_pointer (&self->title, g_free);
        self->title = g_value_dup_string (value);
        break;
      default :
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
rv_video_view_get_property (GObject    *object,
                           guint       property_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  RvVideoView *self = RV_VIDEO_VIEW (object);

  switch ((RvVideoViewProperty) property_id)
    {
      case PROP_VIDEO:
        g_value_set_object (value, self->video);
        break;

      case PROP_WANTS_STACK_SWITCHER:
        g_value_set_boolean (value, self->needs_switcher);
        break;

      case PROP_USE_BOTTOM_SWITCHER:
        g_value_set_boolean (value, self->bottom_switcher);
        break;

      case PROP_VIEW:
        g_value_set_object (value, self->view);
        break;

      case PROP_TITLE:
        g_value_set_string (value, self->title);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
rv_video_view_finalize (GObject *gobject)
{
  RvVideoView *self = RV_VIDEO_VIEW (gobject);

  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (rv_video_view_parent_class)->finalize (gobject);
}

static void
rv_video_view_dispose (GObject *gobject)
{
  RvVideoView *self = RV_VIDEO_VIEW (gobject);

  g_clear_object (&self->video);

  G_OBJECT_CLASS (rv_video_view_parent_class)->dispose (gobject);
}


static void
rv_video_view_class_init (RvVideoViewClass *klass)
{
  GtkWidgetClass *class_widget = GTK_WIDGET_CLASS (klass);
  GObjectClass *class_object = G_OBJECT_CLASS (klass);

  class_object->finalize = rv_video_view_finalize;
  class_object->dispose = rv_video_view_dispose;
  class_object->set_property = rv_video_view_set_property;
  class_object->get_property = rv_video_view_get_property;

  obj_properties[PROP_VIDEO] =
    g_param_spec_object ("video", NULL, NULL,
                         RV_TYPE_VIDEO,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (class_object, PROP_VIDEO, obj_properties[PROP_VIDEO]);

  obj_properties[PROP_STACK] =
    g_param_spec_object ("stack", NULL, NULL,
                         ADW_TYPE_VIEW_STACK,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (class_object, PROP_STACK, obj_properties[PROP_STACK]);

  obj_properties[PROP_USE_BOTTOM_SWITCHER] =
    g_param_spec_boolean ("use-bottom-switcher", NULL, NULL,
                         FALSE,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (class_object, PROP_USE_BOTTOM_SWITCHER, obj_properties[PROP_USE_BOTTOM_SWITCHER]);

  obj_properties[PROP_WANTS_STACK_SWITCHER] =
    g_param_spec_boolean ("wants-stack-switcher", NULL, NULL,
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (class_object, PROP_WANTS_STACK_SWITCHER, obj_properties[PROP_WANTS_STACK_SWITCHER]);

  obj_properties[PROP_VIEW] =
    g_param_spec_object ("view", NULL, NULL,
                         ADW_TYPE_TAB_OVERVIEW,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (class_object, PROP_VIEW, obj_properties[PROP_VIEW]);

  g_object_class_override_property (class_object, PROP_TITLE, "title");

  gtk_widget_class_set_template_from_resource (class_widget, "/app/drey/Receiver/ui/rv-videoview.ui");
  gtk_widget_class_bind_template_child (class_widget, RvVideoView, stack);
  gtk_widget_class_bind_template_child (class_widget, RvVideoView, page_loading);
  gtk_widget_class_bind_template_child (class_widget, RvVideoView, page_video);
  gtk_widget_class_bind_template_child (class_widget, RvVideoView, player);
  gtk_widget_class_bind_template_child (class_widget, RvVideoView, recommended);
}

gboolean
wants_stack_switcher_on_page_video (GBinding *binding,
                                    const GValue *from_value,
                                    GValue *to_value,
                                    gpointer user_data)
{
  RvVideoView *self = RV_VIDEO_VIEW (user_data);
  g_return_val_if_fail (RV_IS_VIDEO_VIEW (self), FALSE);

  GtkWidget *widget = GTK_WIDGET (g_value_get_object (from_value));
  g_return_val_if_fail (GTK_IS_WIDGET (widget), FALSE);

  g_value_set_boolean (to_value, widget == GTK_WIDGET (self->page_video));

  return TRUE;
}

static void
rv_video_view_init (RvVideoView *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_object_bind_property_full (self->stack, "visible-child",
                               self, "wants-stack-switcher",
                               G_BINDING_DEFAULT | G_BINDING_SYNC_CREATE,
                               wants_stack_switcher_on_page_video,
                               NULL, self, NULL);

  GtkExpression *video_expre = gtk_property_expression_new (RV_TYPE_VIDEO_VIEW, NULL, "video");
  GtkExpression *recommended_expre = gtk_property_expression_new (RV_TYPE_VIDEO, video_expre, "recommended");
  gtk_expression_bind (recommended_expre, self->recommended, "src", self);

  GtkExpression *title_expre = gtk_property_expression_new (RV_TYPE_VIDEO, video_expre, "title");
  gtk_expression_bind (title_expre, self, "title", self);
}

RvVideoView*
rv_video_view_new (RvVideo *vid)
{
  return g_object_new (RV_TYPE_VIDEO_VIEW,
                       "video", vid,
                       NULL);
}
