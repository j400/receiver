# Receiver [![Please Don't ship WIP](https://img.shields.io/badge/Please-Don't%20Ship%20WIP-yellow)](https://dont-ship.it/)

It's in early development and not yet ready to use. Have a look at the [goals](GOALS.md) already acomplished and yet to be done.
If you would like to contribute already, keep these things here in mind:
- Don't focus on the High-Level-GUI, it's probably going to be rewritten
- Focus on the already used components in `src/logic`
- Spellcheck, reword, check the grammar and suggest changes to `GUIDELINES.md`, `README.md` and `URI-Spec.txt`
- Fix issues in build-system
- Design GUI/Create mockups.
Please do not work on new features according to the goals without my blessing yet. I'm still figuring out the architecture of the backend.

## Actual preamble

Receiver strives to be like NewPipe on Android but for the GNOME platform!

There are to few people working on libre software. If you would like to help out, look at the [Contributing section](#Contributing).
You don't have to code to contribute to most projects. (Just keep in mind that most things involves coding. Libre projects in general lack a lot of people but a lack of people who can read and write code kills a software-development project.)



## Contributing

There are a lot of things to do, here is an incomplete list:

- Use Receiver, find new bugs and create a [new issue](https://gitlab.gnome.org/j400/receiver/-/issues/new).
- Write new documentation and reword old documentation to adhere to the [GNOME Developer Documentation Style](https://developer.gnome.org/documentation/guidelines/devel-docs.html)
- Fix grammar and spelling mistakes
- Fix bugs from the [bugtracker](https://gitlab.gnome.org/j400/receiver/-/issues)
- Resolve FIXMEs and TODOs.
- Review code
- Design new functionality adhering to the [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/). (Radical changes are welcome.)
- Create an application icon
- Implement auto-completion for the command line.
- Figure something like the [intent filters from NewPipe](https://github.com/TeamNewPipe/NewPipe/blob/ebce4c5b7e437d0d3fc7c3e82f9257f6825bdd03/app/src/main/AndroidManifest.xml#L130) out (relevant links [1](https://developer.gnome.org/documentation/guidelines/maintainer/integrating.html) [2](https://specifications.freedesktop.org/shared-mime-info-spec/).)

The project is in to early development for translations.

Make sure to look at the [Coding Guidelines](GUIDELINES.md) if you want to contribute code.

<!-- FIXME: Should probably be a better link with contact options. Wasn't there a cool gnome page? -->
If you run into problems or can't find something to help with [Jane](https://gitlab.gnome.org/j400) would be happy to help you!


## Building

How to build:

- Use GNOME Builder (and build a Flatpak)
- Run and hope for the best!

For development on Receiver consider adding a custom command to gnome-builder which invokes a video via the [CLI-Interface](#Command-line-interface)

## Command line interface

Receiver has currently an URI interface which can be invoked by just calling Receiver with an URI.

This example starts Receiver from its flatpak, if it's not already open, and opens a youtube video:
```bash
flatpak run app.drey.Receiver receiver:service//youtube/video/dQw4w9WgXcQ
```

The URI-specifaction is in [URI-Spec.txt file](URI-Spec.txt) (changes are welcome, even radical ones, nothing is released and therefore stable yet).

Currently implemented is:
```
receiver:service//youtube/video/[video-id] # opens a youtube video
```

If you would like to see an interface via cli options, create an issue and post an suggestion.


### Submitting things

Make sure the code you're submitting adheres to the [Coding Guidelines](GUIDELINES.md).

The project follows the standard gitlab-merge workflow.
If you would like to send patches, then please open an issue and I'll update this section with an sr.ht mailing-list for them.

### Copyright of assets like pictures and markdown

There is currently no guideline on that and its completly open to debate.
If you're somewhat informed about that topic, then please open an issue.
